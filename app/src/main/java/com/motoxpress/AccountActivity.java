package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Tejsh on 10/3/2016.
 */
public class AccountActivity extends AppCompatActivity {
    Context mContext;
    SweetAlertDialog dialogBuilder;
    ProgressHUD progresshud;
    ResponseTask responseTask;
    String TAG = AccountActivity.class.getSimpleName();
    String Email, AccountNo;
    AlertDialog alertDialog;
    CustomEditText account_account_no;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_account);
        mContext = this;

        account_account_no = (CustomEditText) findViewById(R.id.account_account_no);
        findViewById(R.id.account_newaccount).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCommentDialog();
            }
        });


        account_account_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (account_account_no.length() >= 6) {

                    findViewById(R.id.account_submit_btn).setVisibility(View.VISIBLE);
                }
            }
        });

        findViewById(R.id.account_submit_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AccountNo = account_account_no.getText().toString();
                if (AccountNo.equals("")) {
                    Utility.showToast(mContext, "Please insert your account number");
                } else if (Utility.isConnectingToInternet(mContext)) {
                    AccountPaymentTask();
                } else {
                    Utility.showToast(mContext, "Please check your network connection");
                }

            }
        });
    }

    public void showCommentDialog() {
        dialogBuilder = new SweetAlertDialog(mContext);
        dialogBuilder.setTitle("Please write your comment here");
        dialogBuilder.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                Email = dialogBuilder.getMsg();
                System.out.println("string" + Email);
                if (Email.equals("")) {
                    Utility.ShowToastMessage(mContext, "please provide comment");
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        FillAccountDetailTask();
                    } else {
                        Utility.ShowToastMessage(mContext, "please check your Internet Connection");
                    }
                }
            }
        }).show();

        dialogBuilder.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                // title_text.setText("");

            }
        });

    }

    public void FillAccountDetailTask() {
        progresshud = ProgressHUD.show(mContext, true, false, null);
//https://infograins.com/INFO01/motoxpress/api/api.php?action=Register&fullname=raj&mobile=12&
// email=fsdfhjsdhffkdf&password=56hh&device_type=656&deviceid=877&lat=14&long=4545
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", Email);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.APPLYFORACCOUNT, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                AddQuantityDialog();
                                dialogBuilder.cancel();

                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void AddQuantityDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View convertView = li.inflate(R.layout.dialog_account_email_sent, null);
        alertDialogBuilder.setView(convertView);
        alertDialogBuilder.setCancelable(false);
        alertDialog = alertDialogBuilder.create();
        ((CustomTextView) convertView.findViewById(R.id.account_email_sent)).
                setText(Html.fromHtml(getResources().getString(R.string.send_doc_message)));

        (convertView.findViewById(R.id.ok)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });
        //alertDialog.show();
        /*(convertView.findViewById(R.id.txtdone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                alertDialog.cancel();
            }
        });
*/
        alertDialog.show();
    }

    public void AccountPaymentTask() {
        progresshud = ProgressHUD.show(mContext, true, false, null);

        /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/motoxpress/api/api.php?action=Accountpayment&
        parcel_id=1&user_id=56&payment_method=account&account_no=207036&amount=100&email=hemraj.infograins@gmail.com*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parcel_id", Utility.getSharedPreferences(mContext, Constants.ParcelID));
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("payment_method", "account");
            jsonObject.put("account_no", AccountNo);
            jsonObject.put("amount", Utility.getSharedPreferences(mContext, Constants.Price));
            jsonObject.put("email", Utility.getSharedPreferences(mContext, Constants.EMAIL));
            responseTask = new ResponseTask(mContext, jsonObject, Constants.ACCOUNTPAYMENT, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.showToast(mContext, "Your parcel will delivered soon");
                                startActivity(new Intent(mContext, HomeActivity.class));

                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        finish();
    }
}
