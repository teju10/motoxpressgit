package com.motoxpress;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

/**
 * Created by Tejsh on 8/23/2016.
 */
public class ChangePasswordActivity extends AppCompatActivity {

    public String TAG = ChangePasswordActivity.class.getSimpleName();
    Context mContext;
    String oldpw, newpw, cnfrmpw;
    CustomEditText forgot_oldpw, forget_newdpw, forgot_confirmpw;
    ResponseTask responseTask;
    ProgressHUD progresshud;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);

        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("CHANGE PASSWORD");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        Find();
        Listner();
    }

    public void Find() {

        forgot_oldpw = (CustomEditText) findViewById(R.id.forgot_oldpw);
        forget_newdpw = (CustomEditText) findViewById(R.id.forget_newdpw);
        forgot_confirmpw = (CustomEditText) findViewById(R.id.forgot_confirmpw);
    }

    public void Listner() {

        ((CustomButton) findViewById(R.id.forgot_password_login_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                oldpw = forgot_oldpw.getText().toString();
                newpw = forget_newdpw.getText().toString();
                cnfrmpw = forgot_confirmpw.getText().toString();
                if (newpw.equals("")) {
                    forget_newdpw.setHintTextColor(getResources().getColor(R.color.red));

                } else if (newpw.length() <= 5) {
                    forget_newdpw.setHintTextColor(getResources().getColor(R.color.black));
                    Utility.ShowToastMessage(mContext, "Password length contain atleast 6 digit!!!");

                } else if (cnfrmpw.equals("")) {
                    forgot_confirmpw.setHintTextColor(getResources().getColor(R.color.red));

                } else if (!newpw.equals(cnfrmpw)) {
                    forgot_confirmpw.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "password not matched");
                } else {
                    ChangePasswordTask();
                }
            }
        });
    }

    public void ChangePasswordTask() {

        progresshud = ProgressHUD.show(mContext, true, false, null);
        //https://infograins.com/INFO01/motoxpress/api/api.php?action=ChangePassword&userid=29&newPassword=1234567&oldPassword=123
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("oldPassword", oldpw);
            jsonObject.put("newPassword", newpw);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.CHANGEPASSWORD, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Your Password is changed.");
                                onBackPressed();
                            } else {
                                Utility.showToast(mContext,json.getString("msg"));
                                if(json.getString("msg").equals("Check old password and Enter Correct Old Password"));{
                                    forgot_oldpw.setText("");
                                    forget_newdpw.setText("");
                                    forgot_confirmpw.setText("");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/motoxpress/api/api.php?action=ChangePassword
    &userid=1&newPassword=12&oldPassword=12*/
//    public class ChangePasswordTask extends AsyncTask<String, String, String> {
//        String result = "";
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//            {
//                changepw_dialog.isShown();
//                changepw_dialog.setVisibility(View.VISIBLE);
//            }
//        }
//
//        @Override
//        protected String doInBackground(String... params) {
//
//            ArrayList<NameValuePair> pairArrayList = new ArrayList<NameValuePair>();
//            pairArrayList.add(new BasicNameValuePair("userid", Utility.getSharedPreferences(mContext, Constants.USERID)));
//            pairArrayList.add(new BasicNameValuePair("oldPassword", params[0]));
//            pairArrayList.add(new BasicNameValuePair("newPassword", params[1]));
//
//            result = Utility.postParamsAndfindJSON(Constants.SERVER_URL + Constants.CHANGEPASSWORD, pairArrayList);
//
//
//            return result;
//        }
//
//        @Override
//        protected void onPostExecute(String result) {
//            changepw_dialog.setVisibility(View.GONE);
//            if (result == null) {
//                Utility.ShowToastMessage(mContext, "Server not responding");
//            } else {
//                try {
//                    JSONObject json = new JSONObject(result);
//                    if (json.getString("success").equalsIgnoreCase("1")) {
//                        forgot_oldpw.setText("");
//                        forget_newdpw.setText("");
//                        Utility.showToast(mContext, "Your Password is changed.");
//                    } else {
//                        Utility.ShowToastMessage(mContext, json.getString("msg"));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            super.onPostExecute(result);
//        }
//    }


    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}