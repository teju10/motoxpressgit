package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.fragment.CreditCardUtils;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Infograins on 9/26/2016.
 */
public class CreditCardActivity extends AppCompatActivity {

    CustomEditText credit_card_number, cardexpiry_month, creditcard_cvv;
    Context mContext;
    String TAG = CreditCardActivity.class.getSimpleName(), cardnumber, expirymonth, cvv, strcard_type;
    CustomTextView card_type;
    Integer[] imageArray = {R.drawable.credit_visa, R.drawable.master_card, R.drawable.discover, R.drawable.creditamex};
    ProgressHUD progresshud;
    ResponseTask responseTask;
    String amount="",parcelid="";
    private boolean mValidateCard = true;

    public static ArrayList<String> listOfPattern() {
        ArrayList<String> listOfPattern = new ArrayList<String>();

        String ptVisa = "^4[0-9]$";

        listOfPattern.add(ptVisa);

        String ptMasterCard = "^5[1-5]$";

        listOfPattern.add(ptMasterCard);

        String ptDiscover = "^6(?:011|5[0-9]{2})$";

        listOfPattern.add(ptDiscover);

        String ptAmeExp = "^3[47]$";

        listOfPattern.add(ptAmeExp);

        return listOfPattern;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_with_creditcard);

        mContext = this;
        amount=getIntent().getStringExtra("AMOUNT");
        parcelid=getIntent().getStringExtra(Constants.ParcelID);
        Find();
        Listner();

    }

    public void Find() {
        credit_card_number = (CustomEditText) findViewById(R.id.credit_card_number);
        cardexpiry_month = (CustomEditText) findViewById(R.id.cardexpiry_month);
        creditcard_cvv = (CustomEditText) findViewById(R.id.creditcard_cvv);
        card_type = (CustomTextView) findViewById(R.id.card_type);
    }

    public void Listner() {
        ((CustomButton) findViewById(R.id.submit_creditcard_detail)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardnumber = credit_card_number.getText().toString();
                expirymonth = cardexpiry_month.getText().toString();
                cvv = creditcard_cvv.getText().toString();
                strcard_type = card_type.getText().toString();
                if (cardnumber.equals("")) {
                    Utility.showToast(mContext, "Please add your Credit card number");
                } else if (expirymonth.length()<5) {
                    Utility.showToast(mContext, "Please add your credit card expiry date");
                } else if (cvv.equals("")) {
                    Utility.showToast(mContext, "Please add your cvv");
                } else {
                    final int month=Integer.parseInt(expirymonth.split(CreditCardUtils.SLASH_SEPERATOR)[0]);
                    int year=2000+Integer.parseInt(expirymonth.split(CreditCardUtils.SLASH_SEPERATOR)[1]);
                    Card card = new Card(cardnumber, month, year, cvv);
                    if (!card.validateCard()) {
                        // Show errors
                        Utility.showToast(mContext, "Invalid card details");
                    }else {
                        try{
                            Stripe stripe = new Stripe("pk_test_7JKeM9chawSzwFaDDJAxpa3K");
//                        Stripe stripe = new Stripe("pk_test_L4ICf25c07Rh5avjUMvaPC4o");
                            progresshud = ProgressHUD.show(mContext, true, false, null);
                            stripe.createToken(
                                    card,
                                    new TokenCallback() {
                                        public void onSuccess(Token token) {
                                            System.out.println("token is   "+token);
                                            // Send token to your server
                                            FillAccountDetailTask(token.getId());
//                                        FillAccountDetailTask(token.toString());
                                        }
                                        public void onError(Exception error) {
                                            // Show localized error message
                                            if (progresshud != null && progresshud.isShowing()) {
                                                progresshud.dismiss();
                                            }
                                            Toast.makeText(mContext,
                                                    error.getLocalizedMessage(),
                                                    Toast.LENGTH_LONG
                                            ).show();
                                        }
                                    }
                            );}catch (Exception e){

                        }

                    }
                }
            }
        });

        credit_card_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String ccNum = s.toString();

                if (ccNum.length() >= 2) {
                    for (int i = 0; i < listOfPattern().size(); i++) {
                        if (ccNum.substring(0, 2).matches(listOfPattern().get(i))) {
                            card_type.setCompoundDrawablesWithIntrinsicBounds(0, 0, imageArray[i], 0);

                            String cardtype = String.valueOf(i);
                            if (cardtype.equals("0")) {
                                card_type.setText("VISA");

                            } else if (cardtype.equals("1")) {
                                card_type.setText("MASTER CARD");

                            } else if (cardtype.equals("2")) {
                                card_type.setText("DISCOVER");

                            } else if (cardtype.equals("3")) {
                                card_type.setText("AMERICAN EXPRESS");

                            }
                        }

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                int cursorPosition = credit_card_number.getSelectionEnd();
                int previousLength = credit_card_number.getText().length();

                String cardNumber = CreditCardUtils.handleCardNumber(s.toString());
                int modifiedLength = cardNumber.length();

                credit_card_number.removeTextChangedListener(this);
                credit_card_number.setText(cardNumber);
                credit_card_number.setSelection(cardNumber.length() > CreditCardUtils.MAX_LENGTH_CARD_NUMBER_WITH_SPACES ? CreditCardUtils.MAX_LENGTH_CARD_NUMBER_WITH_SPACES : cardNumber.length());
                credit_card_number.addTextChangedListener(this);

                if (modifiedLength <= previousLength && cursorPosition < modifiedLength) {
                    credit_card_number.setSelection(cursorPosition);
                }

                if(credit_card_number.length()<=0){
                    card_type.setText("");
                    imageArray.equals(null);
                }
            }
        });

        cardexpiry_month.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable ss) {
                try {
                    String text = ss.toString().replace(CreditCardUtils.SLASH_SEPERATOR, "");

                    String month, year = "";
                    if (text.length() >= 2) {
                        month = text.substring(0, 2);

                        if (text.length() > 2) {
                            year = text.substring(2);
                        }

                        if (mValidateCard) {
                            int mm = Integer.parseInt(month);

                            if (mm <= 0 || mm >= 13) {
                                cardexpiry_month.setError("Invalid month");
                                return;
                            }

                            if (text.length() >= 4) {

                                int yy = Integer.parseInt(year);

                                final Calendar calendar = Calendar.getInstance();
                                int currentYear = calendar.get(Calendar.YEAR);
                                int currentMonth = calendar.get(Calendar.MONTH) + 1;

                                int millenium = (currentYear / 1000) * 1000;

                                if (yy + millenium < currentYear) {
                                    cardexpiry_month.setError("Card expired");
                                    return;
                                } else if (yy + millenium == currentYear && mm < currentMonth) {
                                    cardexpiry_month.setError("Card expired");
                                    return;
                                }
                            }
                        }

                    } else {
                        month = text;
                    }

                    int cardexpirypreviousLength = cardexpiry_month.getText().length();
                    int cardexpirycursorPosition = cardexpiry_month.getSelectionEnd();

                    text = CreditCardUtils.handleExpiration(month, year);

                    cardexpiry_month.removeTextChangedListener(this);
                    cardexpiry_month.setText(text);
                    cardexpiry_month.setSelection(text.length());
                    cardexpiry_month.addTextChangedListener(this);

                    int cardexpirymodifiedLength = text.length();

                    if (cardexpirymodifiedLength <= cardexpirypreviousLength && cardexpirycursorPosition < cardexpirymodifiedLength) {
                        cardexpiry_month.setSelection(cardexpirycursorPosition);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void FillAccountDetailTask(String token) {
        //   progresshud = ProgressHUD.show(mContext, true, false, null);
//https://infograins.com/INFO01/motoxpress/api/api.php?action=Register&fullname=raj&mobile=12&
// action=UpdateParcelPaymentStripe&parcelid=103&amount=12&token=35345345
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parcelid", parcelid);
            jsonObject.put("amount", amount);
            jsonObject.put("token", token);
            // jsonObject.put("exp_year", Email);
            // jsonObject.put("card_number", cardnumber);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.UpdateParcelPaymentStripe, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Parcel Accepted");
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            if (progresshud != null && progresshud.isShowing()) {
                progresshud.dismiss();
            }
            e.printStackTrace();
        }
    }

}
