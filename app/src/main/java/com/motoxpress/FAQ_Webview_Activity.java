package com.motoxpress;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.Utility;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

/**
 * Created by Infograins on 9/26/2016.
 */
public class FAQ_Webview_Activity extends AppCompatActivity {
    Context mContext;
    ShimmerTextView shimmertv;
    Shimmer shimmer;
    private WebView webView;
    LinearLayout disconnect;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq_webview);

        mContext = this;
        webView = (WebView) findViewById(R.id.faq_webView);
        shimmertv = (ShimmerTextView) findViewById(R.id.faq_shimmer);
        disconnect = (LinearLayout) findViewById(R.id.disconnect);
        webView.getSettings().setJavaScriptEnabled(true);
        if (Utility.isConnectingToInternet(mContext)) {
            webView.setWebViewClient(new MyBrowser());
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webView.loadUrl("https://infograins.com/INFODE/pankaj/motoxpress/html/app_faq.php");
        } else {
            webView.setVisibility(View.GONE);
            disconnect.setVisibility(View.VISIBLE);
            toggleAnimation();
        }

    }

    public void toggleAnimation() {
        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(shimmertv);
        }
    }

    private class MyBrowser extends WebViewClient {
        ProgressHUD progresshud;

        public MyBrowser() {
            progresshud = ProgressHUD.show(mContext, true,
                    false, null);
        }
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);
            if (progresshud != null && progresshud.isShowing()) {
                progresshud.dismiss();
            }
        }
    }
}
