package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.andexert.library.RippleView;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

/**
 * Created by LakhanPatidar on 19-Aug-16.
 */
public class ForgetPasswordActivity extends AppCompatActivity {

    Context mContext;
    CustomEditText forget_newdpw, forgot_confirmpw;
    String newemailString;
    ResponseTask responseTask;
    ProgressHUD progresshud;
    String TAG = "ResponseTask";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);
        mContext = this;

        forget_newdpw = (CustomEditText) findViewById(R.id.forget_newdpw);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("FORGOT PASSWORD");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Init();
    }

    public void Init() {
        ((RippleView) findViewById(R.id.forgot_password_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                newemailString = forget_newdpw.getText().toString();
                if (newemailString.equals("")) {
                    Utility.ShowToastMessage(mContext, "please enter your email address");
                } else if (!Utility.isValidEmail(newemailString)) {
                    Utility.ShowToastMessage(mContext, "email not valid");
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
//                        Utility.ShowToastMessage(mContext, "coming soon");
                        ForgetPasswordTask(newemailString);
                    } else {
                        Utility.ShowToastMessage(mContext, "Please check your Internet Connection");
                    }
                }
            }
        });
    }

    public void ForgetPasswordTask(String emailString) {
        progresshud = ProgressHUD.show(mContext, true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", emailString);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.FORGOT, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if(result == null){
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try{
                            JSONObject json = new JSONObject(result);
                            if(json.getString("success").equalsIgnoreCase("1")){
                                Utility.ShowToastMessage(mContext, "Your password is sent to your email...");
                                startActivity(new Intent(mContext,LoginActivity.class));
                                finish();
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}

