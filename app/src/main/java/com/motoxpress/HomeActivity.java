package com.motoxpress;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.fragment.AboutUsFragment;
import com.motoxpress.fragment.BillingFragment;
import com.motoxpress.fragment.MyDeliveriesFragment;
import com.motoxpress.fragment.NotificationListFragment;
import com.motoxpress.fragment.ProfileFragment;
import com.motoxpress.fragment.SendParcelFragment;
import com.motoxpress.fragment.SettingFragment;
import com.motoxpress.navdrawer.FragmentDrawer;
import com.motoxpress.utility.Utility;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejsh on 07-Jul-16.
 */
public class HomeActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, View.OnClickListener {

    public static final String mBroadcastString = "mynotification";
    public static String isopenHome;
    Context mContext;
    Toolbar toolbar;
    FragmentDrawer drawerFragment;
    DrawerLayout mDrawerLayout;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    String Notification;
    private IntentFilter mIntentFilter;

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(mBroadcastString)) {
                Checknotification();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mContext = this;
        init();
        bind();

        Notification = getIntent().getStringExtra(Constants.MYDELIVERYFRAGMENT);
        if (Notification != null && !Notification.equals("")) {
            if (Notification.equals("my_delivery")) {
                CallSelectedFragment(Constants.MYDELIVERYFRAGMENT, "MY DELIVERY");
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            registerReceiver(mReceiver, mIntentFilter);
            Checknotification();
        } catch (Exception e) {

        }
    }

    @Override
    protected void onPause() {
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onPause();
    }

    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
       /* navigationView = (NavigationView) findViewById(R.id.navigation_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);*/
    }

    public void bind() {
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment.setDrawerListener(this);
        (mDrawerLayout.findViewById(R.id.drawer_profile)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.drawer_inbox)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.drawer_setting)).setOnClickListener(this);
        (mDrawerLayout.findViewById(R.id.red_dot_icon)).setOnClickListener(this);
        displayView(0);
        isopenHome = "222";
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(mBroadcastString);
        Checknotification();
    }

    @Override
    protected void onDestroy() {
        isopenHome = null;
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }


    public void displayView(int position) {
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        if (mDrawerLayout.isDrawerOpen((View) findViewById(R.id.fragment_navigation_drawer))) {
            mDrawerLayout.closeDrawer((View) findViewById(R.id.fragment_navigation_drawer));
        }
        switch (position) {
            case 0:
                fragment = new SendParcelFragment();
                title = getString(R.string.sendparcel);
                break;
            case 1:
                fragment = new MyDeliveriesFragment();
                title = getString(R.string.mydelivery);
                break;
            case 2:
                fragment = new BillingFragment();
                title = getString(R.string.title_billing);
                break;

            case 3:
                fragment = new AboutUsFragment();
                title = getString(R.string.title_aboutus);
                break;
            case 4:
                LogoutAlert();
                title = getString(R.string.d_logout);
                break;

            case 6:
                fragment = new ProfileFragment();
                title = getString(R.string.drawer_profilename);
                break;
            case 7:
                fragment = new NotificationListFragment();
                title = getString(R.string.title_notification);
                break;

            case 8:
                fragment = new SettingFragment();
                title = getString(R.string.drawer_setting);
                break;

            default:
                break;
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            ((CustomTextView) toolbar.findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_profile:
                if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {
                    displayView(6);
                } else {
                    Utility.ShowToastMessage(mContext, "You need to login first");
                }
                break;
            case R.id.drawer_inbox:
                Utility.setSharedPreference(getApplicationContext(), "SET", "");
                (mDrawerLayout.findViewById(R.id.red_dot_icon)).setVisibility(View.GONE);
                displayView(7);
                break;
            case R.id.drawer_setting:
                displayView(8);
                break;
        }
    }

    public void Checknotification() {
        if (Utility.getSharedPreferences(mContext, "SET").equals("")) {
            (mDrawerLayout.findViewById(R.id.red_dot_icon)).setVisibility(View.GONE);
        } else {
            (mDrawerLayout.findViewById(R.id.red_dot_icon)).setVisibility(View.VISIBLE);
        }

    }

   /* @Override
    public void onBackPressed() {

        if (drawerFragment.isVisible()) {
            mDrawerLayout.closeDrawers();
        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.RP_GiveReplyEdit_ListFragment)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(1);

        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.RecentPostsListFragment)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(1);

        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.HomeFragment)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(0);

        } else if (Utility.getSharedPreferences(mContext, Constants.BACKPRESSEDNAME).equals(Constants.Home_Topics_in_CategoryList_Fragment)) {
            Utility.removepreference(mContext, Constants.BACKPRESSEDNAME);
            displayView(0);

        } else {
            finish();
        }
    }*/

    public void CallSelectedFragment(String FragmentName, String Title) {
        Fragment fragment = null;
        String title;
        if (FragmentName.equals(Constants.MYDELIVERYFRAGMENT)) {
            fragment = new MyDeliveriesFragment();
        }
        title = Title;
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
            ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(title);
        }
    }

    public void LogoutAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Logout!");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage("Do you want to logout?");
        builder.setCancelable(false);
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                new LogoutTask().execute();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.dismiss();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public class LogoutTask extends AsyncTask<String, String, String> {
        String result = "";

        @Override
        protected String doInBackground(String... params) {
            // progressHUD = ProgressHUD.show(mContext, true, false, null);
            ArrayList<NameValuePair> pairArrayList = new ArrayList<>();
            System.out.println("MYUSRID " + Utility.getSharedPreferences(mContext, Constants.USERID));
            pairArrayList.add(new BasicNameValuePair("userid", Utility.getSharedPreferences(mContext, Constants.USERID)));
            result = Utility.postParamsAndfindJSON(Constants.SERVER_URL + Constants.LOGOUT, pairArrayList);
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            /*if (progressHUD.isShowing()) {
                progressHUD.dismiss();
            }*/
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Server not responding");
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equalsIgnoreCase("1")) {
                        Utility.clearsharedpreference(mContext);
                        Utility.clearsharedpreference(mContext);
                        Utility.showToast(mContext, "Logout Successfully");
                        Intent logout = new Intent(mContext, MainActivity.class);
                        startActivity(logout);
                        finish();

                    } else {
                        Utility.ShowToastMessage(mContext, json.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result);
        }
    }

}