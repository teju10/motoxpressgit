package com.motoxpress;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.andexert.library.RippleView;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.GPSTracker;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

/**
 * Created by Tejsh on 8/17/2016.
 */
public class LoginActivity extends AppCompatActivity {

    public String TAG = LoginActivity.class.getSimpleName();
    Context mContext;
    double latitude = 0.0, longitude = 0.0;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    CustomEditText login_email, login_password;
    int flag = 0;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    RippleView loginbtn;
    String emailString, passwordString;
    ProgressHUD progresshud;
    CustomButton verify_account_btn;
    ResponseTask responseTask;
    Dialog dialog_verification;
    CustomEditText commet_review;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        share = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);


        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("LOGIN");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        init();
        bind();
        Listeners();

    }

    public void init() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(mContext);
        }
        gpsTracker = new GPSTracker(mContext);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
       /* if (Utility.getSharedPreferences(mContext, "GCMID").equals("")) {
            GCMIntentService.registerForNotification(LoginActivity.this);
        }
*/
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
    }

    public void bind() {
        login_email = (CustomEditText) findViewById(R.id.login_email);
        login_password = (CustomEditText) findViewById(R.id.login_password);
        loginbtn = (RippleView) findViewById(R.id.ripple_login_parent);
        verify_account_btn = (CustomButton) findViewById(R.id.verify_account_btn);
    }

    public void Listeners() {

        ((RippleView) findViewById(R.id.login_forgot_password)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ForgetPasswordActivity.class);
                startActivity(intent);
            }
        });

        ((RippleView) findViewById(R.id.login_signup)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SignupActivity.class);
                startActivity(intent);
            }
        });

        verify_account_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verificationAlert();
            }
        });

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailString = login_email.getText().toString();
                passwordString = login_password.getText().toString();

                if (emailString.equals("")) {
                    login_email.setHintTextColor(getResources().getColor(R.color.red));
                    // Utility.ShowToastMessage(mContext, "email is empty");
                } else if (!Utility.isValidEmail(emailString)) {
                    login_email.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "email not valid");
                } else if (passwordString.equals("")) {
                    login_password.setHintTextColor(getResources().getColor(R.color.red));
                    // Utility.ShowToastMessage(mContext, "password is empty");
                } else if (latitude == 0.0 || longitude == 0.0) {
                    Utility.AlertNoGPS(mContext);
                } else {
                    LoginTask();
                }
            }
        });
    }

    public void verificationAlert() {
        dialog_verification = new Dialog(mContext);
        dialog_verification.setCancelable(false);
        dialog_verification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_verification.setContentView(R.layout.dialog_varification);
        dialog_verification.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        commet_review = (CustomEditText) dialog_verification.findViewById(R.id.commet_review);

        (dialog_verification.findViewById(R.id.skip_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_verification.dismiss();
            }
        });
        (dialog_verification.findViewById(R.id.review_submit)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (commet_review.getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "please enter verification code");
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        VerificationTask(commet_review.getText().toString());
                        dialog_verification.dismiss();
                    } else {
                        Utility.ShowToastMessage(mContext, "Check your internet connection");
                    }
                }
            }
        });
        dialog_verification.show();
    }

    public void VerificationTask(String code) {
        progresshud = ProgressHUD.show(mContext, true, false, null);
//https://infograins.com/INFO01/motoxpress/api/api.php?action=Register&fullname=raj&mobile=12&
// email=fsdfhjsdhffkdf&password=56hh&device_type=656&deviceid=877&lat=14&long=4545
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", code);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.VerifyAccount, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Verification Completed please login");
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void LoginTask() {
        progresshud = ProgressHUD.show(mContext, true, false, null);
//https://infograins.com/INFO01/motoxpress/api/api.php?action=Register&fullname=raj&mobile=12&
// email=fsdfhjsdhffkdf&password=56hh&device_type=656&deviceid=877&lat=14&long=4545
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("email", emailString);
            jsonObject.put("password", passwordString);
            jsonObject.put("device_type", "android");
            jsonObject.put("deviceid", Utility.getSharedPreferences(mContext, Constants.FCMID));
            responseTask = new ResponseTask(mContext, jsonObject, Constants.LOGIN, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.showToast(mContext, "Login successfully");
                                JSONObject jsonObject1 = json.getJSONObject("userdetails");
                                Utility.setSharedPreference(mContext, Constants.SPLOGIN, "1");
                                Utility.setSharedPreference(mContext, Constants.USERID, jsonObject1.getString("userid"));
                                Utility.setSharedPreference(mContext, Constants.FULLNAME, jsonObject1.getString("fullname"));
                                Utility.setSharedPreference(mContext, Constants.EMAIL, jsonObject1.getString("email"));
                                if (jsonObject1.getString("noti_status").equals("0")) {
                                    Utility.setSharedPreference(mContext, Constants.NOTIFICATION_STATUS, "0");
                                } else if (jsonObject1.getString("noti_status").equals("1")) {
                                    Utility.setSharedPreference(mContext, Constants.NOTIFICATION_STATUS, "1");
                                }
                                Utility.setSharedPreference(mContext, Constants.USER_TYPE, json.getString("usertype"));
                                startActivity(new Intent(mContext, HomeActivity.class));
                                finish();
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(mContext, MainActivity.class));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}