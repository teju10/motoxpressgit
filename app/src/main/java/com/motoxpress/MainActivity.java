package com.motoxpress;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;

import com.andexert.library.RippleView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.motoxpress.constants.Constants;
import com.motoxpress.utility.Utility;

public class MainActivity extends AppCompatActivity {

    public String TAG = MainActivity.class.getSimpleName();
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        Find();
        Initialize();
    }

    public void Find() {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String IMEINumber = telephonyManager.getDeviceId();
        Utility.setSharedPreference(mContext, Constants.IMEINUMBER, IMEINumber);
        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(getApplicationContext(), Constants.FCMID, token);
        }

    }

    public void Initialize() {
        ((RippleView) findViewById(R.id.main_loginbtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
        ((RippleView) findViewById(R.id.main_signupbtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, SignupActivity.class);
                startActivity(i);
                finish();
            }
        });
        ((RippleView) findViewById(R.id.main_skipbtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mContext, HomeActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    public void LogoutAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Logout!");
        builder.setIcon(R.drawable.ic_launcher);
        builder.setMessage("Do you want to logout?")
                .setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                Utility.clearsharedpreference(mContext);
                Intent logout = new Intent(mContext, LoginActivity.class);
                startActivity(logout);
                finish();
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
                dialog.dismiss();
                finish();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
