package com.motoxpress;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 9/30/2016.
 */
public class NotificationdetailActivity extends AppCompatActivity {

    Context mContext;
    Bundle b;
    ProgressHUD progress;
    ResponseTask responseTask;
    String TAG = "NotificationdetailActivity", NotiStatus;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        setContentView(R.layout.notificationdetailactivity);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.title_notification));
        try {

            b = new Bundle();
            b = getIntent().getExtras();

            NotiStatus = b.getString("noti_status");
            ((CustomTextView) findViewById(R.id.notify_km)).setText(b.getString("total_distance") + " km");
            ((CustomTextView) findViewById(R.id.mydelivery_amount)).setText("$ " + b.getString("total_cost"));
            ((CustomTextView) findViewById(R.id.mydelivery_date)).setText(b.getString("boking_time"));
            ((CustomTextView) findViewById(R.id.mydelivery_pickup)).setText(b.getString("total_distance"));

            if ((b.getString("delivery_status").equals("accepting"))) {
                ((CustomTextView) findViewById(R.id.parcel_status)).setText("Parcel is accepted");
            }
            else if (b.getString("delivery_status").equals("pending")) {
                ((CustomTextView) findViewById(R.id.parcel_status)).setText("Pending");
            }
            else if (b.getString("delivery_status").equals("picking")) {
                ((CustomTextView) findViewById(R.id.parcel_status)).setText("your parcel is picked by our team");
            }
            else if (b.getString("delivery_status").equals("running")) {
                ((CustomTextView) findViewById(R.id.parcel_status)).setText("Running");
            }
            else if (b.getString("delivery_status").equals("completing")) {
                ((CustomTextView) findViewById(R.id.parcel_status)).setText("Parcel is delivered ");
            }
            ((CustomTextView) findViewById(R.id.mydelivery_pickup)).setText(b.getString("pickup_address"));
            ((CustomTextView) findViewById(R.id.mydelivery_dropup)).setText(b.getString("dropof_address"));
            ((CustomTextView) findViewById(R.id.contact_number)).setText(b.getString("runnermobile"));

            System.out.println(TAG +"NOTIII_STATUS "+NotiStatus);
            if (NotiStatus.equals("0")){
                ReadUnreadCountingTask();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public void ReadUnreadCountingTask() {
        try {
            progress = ProgressHUD.show(mContext, true, false, null);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("parcelid", b.getString("parcel_id"));
            jsonObject.put("noti_id", b.getString("noti_id"));
            responseTask = new ResponseTask(mContext, jsonObject, Constants.PARCELDRIVERSTATUS, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progress.equals("") && progress.isShowing()) {
                        progress.dismiss();
                    }
                    else if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
