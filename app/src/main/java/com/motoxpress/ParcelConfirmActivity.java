package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.andexert.library.RippleView;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Tejsh on 9/8/2016.
 */
public class ParcelConfirmActivity extends AppCompatActivity implements View.OnClickListener, TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    String TAG = ParcelConfirmActivity.class.getSimpleName();
    Context mContext;
    LinearLayout express_ll, delivery_ll, delivery2_ll, parcel_pickup_delivery_12, parcel_pickup_delivery_1,
            parcel_pickup_delivery_2, parcel_pickup_delivery_4;
    CustomTextView one_hourexpress, one_hour, two_hour, two_hourdelivery, four_hour, four_hourdelivery,
            parcel_pickup_delivery_12_time, parcel_pickup_delivery_1_time, parcel_pickup_delivery_2_time,
            parcel_pickup_delivery_4_time, parcel_pickup_delivery_12_price, parcel_pickup_delivery_1_price,
            parcel_pickup_delivery_2_price, parcel_pickup_delivery_4_price, price_express, price_2_3express, price_4_5express;
    CheckBox mode24Hours;
    Bundle bundle;
    Calendar now = Calendar.getInstance();
    DatePickerDialog dpd;
    TimePickerDialog tpd;
    ResponseTask responseTask;
    ArrayList<JSONObject> expressArrayList, simpleArrayList;
    float FlatRate = 0, KMRate = 0, Price = 0, itemCountPrice = 0;
    int delivery_type = 0;
    String pickupDate, pickupTime, dropTime, dropDate,StrPayment;
    boolean is_pickup = false;
    ProgressHUD progresshud;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_confirm_delivery);

        mContext = this;
        bind();
        listner();
    }

    public void bind() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.confirm));
        ((CustomTextView) findViewById(R.id.title)).setText(getResources().getString(R.string.summary));
        one_hourexpress = (CustomTextView) findViewById(R.id.one_hourexpress);
        one_hour = (CustomTextView) findViewById(R.id.one_hour);
        two_hour = (CustomTextView) findViewById(R.id.two_hour);
        two_hourdelivery = (CustomTextView) findViewById(R.id.two_hourdelivery);
        four_hour = (CustomTextView) findViewById(R.id.four_hour);
        four_hourdelivery = (CustomTextView) findViewById(R.id.four_hourdelivery);
      //  mode24Hours = (CheckBox) findViewById(R.id.mode_24_hours);

        express_ll = (LinearLayout) findViewById(R.id.express_ll);
        delivery_ll = (LinearLayout) findViewById(R.id.delivery_ll);
        delivery2_ll = (LinearLayout) findViewById(R.id.delivery2_ll);

        parcel_pickup_delivery_12 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_12);
        parcel_pickup_delivery_1 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_1);
        parcel_pickup_delivery_2 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_2);
        parcel_pickup_delivery_4 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_4);

        parcel_pickup_delivery_12_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_12_time);
        parcel_pickup_delivery_1_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_1_time);
        parcel_pickup_delivery_2_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_2_time);
        parcel_pickup_delivery_4_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_4_time);
        parcel_pickup_delivery_12_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_12_price);
        parcel_pickup_delivery_1_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_1_price);
        parcel_pickup_delivery_2_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_2_price);
        parcel_pickup_delivery_4_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_4_price);

        price_express = (CustomTextView) findViewById(R.id.price_express);
        price_2_3express = (CustomTextView) findViewById(R.id.price_2_3express);
        price_4_5express = (CustomTextView) findViewById(R.id.price_4_5express);

        ((LinearLayout) findViewById(R.id.center_layout)).setVisibility(View.GONE);

        bundle = new Bundle();
        bundle = getIntent().getExtras();
        dpd = DatePickerDialog.newInstance(ParcelConfirmActivity.this, now.get(Calendar.YEAR), now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH));
        dpd.setMinDate(now);
        dpd.registerOnDateChangedListener(new DatePickerDialog.OnDateChangedListener() {
            @Override
            public void onDateChanged() {

            }
        });
        tpd = TimePickerDialog.newInstance(ParcelConfirmActivity.this, 0, 0, false);
        GetPriceForDelivery("1");
    }

    public void listner() {

        express_ll.setOnClickListener(this);
        delivery_ll.setOnClickListener(this);
        delivery2_ll.setOnClickListener(this);

        ((CustomTextView) findViewById(R.id.confirm_estimated_tripdistance)).setText(bundle.getString(Constants.Distance) + " KM");

       /* try {
            if (expressArrayList.get(0).getString("name").contains("1H")) {
                FlatRate = Float.parseFloat(expressArrayList.get(0).getString("flatrate"));
                KMRate = Float.parseFloat(expressArrayList.get(0).getString("kmrate"));
                float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                    itemCountPrice = Price * 70 / 100;
                    itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                    Price = itemCountPrice + Price;
                    ((CustomTextView) findViewById(R.id.price_express)).setText("AU$ " + String.format("%.2f", Price));
                } else if (expressArrayList.get(0).getString("name").contains("2-3H"))
                    FlatRate = Float.parseFloat(expressArrayList.get(1).getString("flatrate"));
                KMRate = Float.parseFloat(expressArrayList.get(1).getString("kmrate"));
                float diatance1 = Float.parseFloat(bundle.getString(Constants.Distance));
                Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance1)) * KMRate);

                if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                    itemCountPrice = Price * 70 / 100;
                    itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                    Price = itemCountPrice + Price;
                    ((CustomTextView) findViewById(R.id.price_2_3express)).setText("AU$ " + String.format("%.2f", Price));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/

        ((CustomTextView) findViewById(R.id.confirm_pickup_at)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_pickup = true;
                dpd.setMinDate(now);
                dpd.show(getFragmentManager(), "Date picker");
            }
        });

        ((RippleView) findViewById(R.id.ripple_choose_another_date_time)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                is_pickup = false;
                String[] token = pickupDate.split("/");
                if (Integer.parseInt(token[0]) > now.get(Calendar.DAY_OF_MONTH) &&
                        (Integer.parseInt(token[1]) - 1) >= now.get(Calendar.MONTH) &&
                        Integer.parseInt(token[2]) >= now.get(Calendar.YEAR)) {

                    Log.e(TAG, "day ==> " + Integer.parseInt(token[0]) + " month ==> " + Integer.parseInt(token[1]) +
                            " year ==> " + Integer.parseInt(token[2]));

                    int day = Integer.parseInt(token[0]) - now.get(Calendar.DAY_OF_MONTH);
                    int month = (Integer.parseInt(token[1]) - 1) - now.get(Calendar.MONTH);
                    int year = Integer.parseInt(token[2]) - now.get(Calendar.YEAR);

                    Log.e(TAG, "day d==> " + day + " month d==>" + month + " year d==> " + year);

                    Calendar calendar = Calendar.getInstance();
                    calendar.add(Calendar.DAY_OF_MONTH, day);
                    calendar.add(Calendar.MONTH, month);
                    calendar.add(Calendar.YEAR, year);

                    dpd.setMinDate(calendar);
                    dpd.show(getFragmentManager(), "Date picker");
                } else {
                    dpd.show(getFragmentManager(), "Date picker");
                }
            }
        });

        ((RippleView) findViewById(R.id.ripple_choose_another_date)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((LinearLayout) findViewById(R.id.parcel_pickup_delivery_time_layout)).setVisibility(View.VISIBLE);
                GetPriceForDelivery("0");
                ((LinearLayout) findViewById(R.id.select_parcel_type)).setVisibility(View.GONE);
                //((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.GONE);
                ((RippleView) findViewById(R.id.ripple_choose_another_date_time)).setVisibility(View.VISIBLE);
                ((RippleView) findViewById(R.id.ripple_choose_another_date)).setVisibility(View.GONE);
            }
        });
        changeColorLayout();

        ((CustomButton) findViewById(R.id.next)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (((CustomTextView) findViewById(R.id.confirm_pickup_at)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "Please select pickup time");
                } else if (((CustomTextView) findViewById(R.id.confirm_delivered_by)).getText().toString().equals("")) {
                    Utility.ShowToastMessage(mContext, "Please select drop off time");
                } else {
                    Intent intent = new Intent(mContext, ParcelConfirmActivity2.class);
                    bundle.putString(Constants.PickUpTime, ((CustomTextView) findViewById(R.id.confirm_pickup_at)).getText().toString());
                    bundle.putString(Constants.DropOffTime, ((CustomTextView) findViewById(R.id.confirm_delivered_by)).getText().toString());

                    StrPayment = (((CustomTextView) findViewById(R.id.parcel_confirm_price)).getText().toString()).replace("AU$"," ").trim();
                    bundle.putString(Constants.Price, StrPayment);
                    System.out.println("Payment " + StrPayment);
                    bundle.putString(Constants.DeliveryType, String.valueOf(delivery_type));
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    public void changeColorLayout() {
        parcel_pickup_delivery_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }

                ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText(
                        parcel_pickup_delivery_12_price.getText().toString());
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText(getResources().getString(R.string.by_12_tomorrow));
                try {
                    delivery_type = Integer.parseInt(simpleArrayList.get(0).getString("id"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        parcel_pickup_delivery_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }

                ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText(
                        parcel_pickup_delivery_1_price.getText().toString());
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText(getResources().getString(R.string.by_1_tomorrow));

                try {
                    delivery_type = Integer.parseInt(simpleArrayList.get(1).getString("id"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        parcel_pickup_delivery_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }

                ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText(
                        parcel_pickup_delivery_2_price.getText().toString());
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText(getResources().getString(R.string.by_2_tomorrow));
                //System.out.println("parcel_pickup_delivery_2_price "+parcel_pickup_delivery_2_price.getText().toString());
                try {
                    delivery_type = Integer.parseInt(simpleArrayList.get(2).getString("id"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        parcel_pickup_delivery_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.white));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.white));

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                }

                ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText(
                        parcel_pickup_delivery_4_price.getText().toString());
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText(getResources().getString(R.string.by_4_tomorrow));
                try {
                    delivery_type = Integer.parseInt(simpleArrayList.get(3).getString("id"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.express_ll:
                onOneHourExpress();
                break;
            case R.id.delivery_ll:
                onTwoHourExpress();
                break;
            case R.id.delivery2_ll:
                onThreeHourExpress();
                break;
        }
    }

    public void onOneHourExpress() {
        express_ll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        delivery_ll.setBackgroundColor(getResources().getColor(R.color.white));
        delivery2_ll.setBackgroundColor(getResources().getColor(R.color.white));
        one_hour.setTextColor(getResources().getColor(R.color.white));
        one_hourexpress.setTextColor(getResources().getColor(R.color.white));

        price_express.setTextColor(getResources().getColor(R.color.white));
        price_2_3express.setTextColor(getResources().getColor(R.color.colorPrimary));
        price_4_5express.setTextColor(getResources().getColor(R.color.colorPrimary));

        two_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        two_hourdelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
        four_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        four_hourdelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
        (findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);

        oneHour();
    }

    public void onTwoHourExpress() {
        express_ll.setBackgroundColor(getResources().getColor(R.color.white));
        delivery_ll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        delivery2_ll.setBackgroundColor(getResources().getColor(R.color.white));
        one_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        one_hourexpress.setTextColor(getResources().getColor(R.color.colorPrimary));
        two_hour.setTextColor(getResources().getColor(R.color.white));

        price_express.setTextColor(getResources().getColor(R.color.colorPrimary));
        price_2_3express.setTextColor(getResources().getColor(R.color.white));
        price_4_5express.setTextColor(getResources().getColor(R.color.colorPrimary));

        two_hourdelivery.setTextColor(getResources().getColor(R.color.white));
        four_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        four_hourdelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
        (findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);

        twoHour();
    }

    public void onThreeHourExpress() {
        express_ll.setBackgroundColor(getResources().getColor(R.color.white));
        delivery_ll.setBackgroundColor(getResources().getColor(R.color.white));
        delivery2_ll.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        one_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        one_hourexpress.setTextColor(getResources().getColor(R.color.colorPrimary));

        price_express.setTextColor(getResources().getColor(R.color.colorPrimary));
        price_2_3express.setTextColor(getResources().getColor(R.color.colorPrimary));
        price_4_5express.setTextColor(getResources().getColor(R.color.white));

        two_hour.setTextColor(getResources().getColor(R.color.colorPrimary));
        two_hourdelivery.setTextColor(getResources().getColor(R.color.colorPrimary));
        four_hour.setTextColor(getResources().getColor(R.color.white));
        four_hourdelivery.setTextColor(getResources().getColor(R.color.white));
        (findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);

        threeHour();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        Log.e(TAG, "hourOfDay ==> " + hourOfDay + " minute ==> " + minute);

        String hourString = "";
        String AM_PM = "";
        if (hourOfDay == 0) {
            hourString = "12";
            AM_PM = "AM";
        } else if (hourOfDay < 12) {
            hourString = "0" + hourOfDay;
            AM_PM = "AM";
        } else if (hourOfDay == 12) {
            hourString = "" + hourOfDay;
            AM_PM = "PM";
        } else if (hourOfDay > 12) {
            hourString = "" + (hourOfDay - 12);
            AM_PM = "PM";
        }

        /*if (minute == 0) {
            minuteString = "00";
        } else if (minute < 30) {
            minuteString = "30";
        } else if (minute > 30 && hourOfDay < 12) {
            minuteString = "00";
            hourString = "" + (hourOfDay + 1);
        } else if (minute > 30 && hourOfDay == 12) {
            minuteString = "00";
            hourString = "" + (hourOfDay + 1);
        } else if (minute > 30 && hourOfDay > 12) {
            minuteString = "00";
            hourString = "" + ((hourOfDay + 1) - 12);
        }*/

        if (is_pickup) {
            if (minute < 10) {
                pickupTime = hourString + ":0" + minute + " " + AM_PM;
            } else {
                pickupTime = hourString + ":" + minute + " " + AM_PM;
            }
            Log.e(TAG, "pickupTime  =======> " + pickupTime);
            ((LinearLayout) findViewById(R.id.center_layout)).setVisibility(View.VISIBLE);
            ((CustomTextView) findViewById(R.id.confirm_pickup_at)).setText(pickupTime + " " + pickupDate);
        } else {
            if (minute < 10) {
                dropTime = hourString + ":0" + minute + " " + AM_PM;
            } else {
                dropTime = hourString + ":" + minute + " " + AM_PM;
            }
            Log.e(TAG, "dropTime  =======> " + dropTime);
            try {
                if (simpleArrayList.get(4).getString("name").contains("Another")) {
                    FlatRate = Float.parseFloat(simpleArrayList.get(4).getString("flatrate"));
                    KMRate = Float.parseFloat(simpleArrayList.get(4).getString("kmrate"));
                    float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                    Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                    if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                        itemCountPrice = Price * 70 / 100;
                        itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                        itemCountPrice = itemCountPrice * (2 - 1);
                        Price = itemCountPrice + Price;
                        ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("$ " + String.format("%.2f", Price));
                    } else {
                        ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("$ " + String.format("%.2f", Price));
                    }
                }
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText(dropTime + " " + dropDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {

        if (is_pickup) {
            pickupDate = dayOfMonth + "" + "/" + (++monthOfYear) + "/" + year;
            Log.e(TAG, "DATE  =======> " + pickupDate);
            view.dismiss();
            if (dayOfMonth == now.get(Calendar.DAY_OF_MONTH) && (--monthOfYear) == now.get(Calendar.MONTH) &&
                    year == now.get(Calendar.YEAR)) {
                tpd.setMinTime(now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), now.get(Calendar.SECOND));
                tpd.show(getFragmentManager(), "Time picker");
            } else {
                tpd.setMinTime(0, 0, 0);
                tpd.show(getFragmentManager(), "Time picker");
            }
        } else {
            dropDate = dayOfMonth + "" + "/" + (++monthOfYear) + "/" + year;
            Log.e(TAG, "DATE  =======> " + dropDate);
            view.dismiss();
            if (dayOfMonth >= now.get(Calendar.DAY_OF_MONTH) && (--monthOfYear) >= now.get(Calendar.MONTH) &&
                    year >= now.get(Calendar.YEAR)) {
                String[] token = pickupTime.split(" ");
                String[] token2 = token[0].split(":");
                int hour = 0;
                int min = 0;
                hour = Integer.parseInt(token2[0]);
                min = Integer.parseInt(token2[1]);
                if (token[1].equalsIgnoreCase("AM")) {
                    hour = Integer.parseInt(token2[0]);
                } else if (token[1].equalsIgnoreCase("PM")) {
                    hour = Integer.parseInt(token2[0]) + 1 + 12;
                }
                tpd.setMinTime(hour, min, 0);
                tpd.show(getFragmentManager(), "Time picker");
            } else {
                tpd.show(getFragmentManager(), "Time picker");
            }
        }
    }

    public void oneHour() {
        try {
            if (expressArrayList.get(0).getString("name").contains("1H")) {
                FlatRate = Float.parseFloat(expressArrayList.get(0).getString("flatrate"));
                KMRate = Float.parseFloat(expressArrayList.get(0).getString("kmrate"));
                float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                    itemCountPrice = Price * 70 / 100;
                    itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                    Price = itemCountPrice + Price;
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                } else {
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                }
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText("Within One Hour");
            }
            delivery_type = Integer.parseInt(expressArrayList.get(0).getString("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void twoHour() {
        try {
            if (expressArrayList.get(1).getString("name").contains("2-3H")) {
                FlatRate = Float.parseFloat(expressArrayList.get(1).getString("flatrate"));
                KMRate = Float.parseFloat(expressArrayList.get(1).getString("kmrate"));
                float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);
                if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                    itemCountPrice = Price * 70 / 100;
                    itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                    Price = itemCountPrice + Price;
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_2_3express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                } else {
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_2_3express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                }
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText("Within 2-3 Hour");
            }
            delivery_type = Integer.parseInt(expressArrayList.get(1).getString("id"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void threeHour() {
        try {
            if (expressArrayList.get(2).getString("name").contains("4-5H")) {
                FlatRate = Float.parseFloat(expressArrayList.get(2).getString("flatrate"));
                KMRate = Float.parseFloat(expressArrayList.get(2).getString("kmrate"));
                float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                Price = FlatRate + (diatance * KMRate);
                if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                    itemCountPrice = Price * 70 / 100;
                    itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                    Price = itemCountPrice + Price;
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_4_5express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                } else {
//                    ((LinearLayout) findViewById(R.id.parcel_confirm_price_layout)).setVisibility(View.VISIBLE);
                    ((CustomTextView) findViewById(R.id.price_4_5express)).setText("AU$ " + String.format("%.2f", Price));
                    ((CustomTextView) findViewById(R.id.parcel_confirm_price)).setText("AU$ " + String.format("%.2f", Price));
                }
                ((CustomTextView) findViewById(R.id.confirm_delivered_by)).setText("Within 4-5 Hour");
            }
            delivery_type = Integer.parseInt(expressArrayList.get(2).getString("id"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetPriceForDelivery(final String type) {
        progresshud = ProgressHUD.show(mContext, true, false, null);
        try {
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", type);
            responseTask = new ResponseTask(ParcelConfirmActivity.this, jsonObject, Constants.GET_DELIVERY_TYPE, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else if (result.equalsIgnoreCase("SocketTimeoutException")) {
                        GetPriceForDelivery(type);
                    } else {
                        try {
                            JSONObject jsonObject1 = new JSONObject(result);
                            if (jsonObject1.getString("success").equalsIgnoreCase("1")) {
                                if (type.equals("1")) {
                                    Log.e(TAG, "GetPriceForDelivery ======> Express");
                                    expressArrayList = new ArrayList<JSONObject>();
                                    JSONArray jsonArray = jsonObject1.getJSONArray("List");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        expressArrayList.add(jsonArray.getJSONObject(i));
                                    }

                                    oneHour();
                                    twoHour();
                                    threeHour();
                                } else {
                                    Log.e(TAG, "GetPriceForDelivery ======> By time");
                                    simpleArrayList = new ArrayList<JSONObject>();
                                    JSONArray jsonArray = jsonObject1.getJSONArray("List");
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        simpleArrayList.add(jsonArray.getJSONObject(i));

                                        if (i == 0) {
                                            parcel_pickup_delivery_1_time.setText(jsonArray.getJSONObject(0).getString("name"));
                                            FlatRate = Float.parseFloat(jsonArray.getJSONObject(0).getString("flatrate"));
                                            KMRate = Float.parseFloat(jsonArray.getJSONObject(0).getString("kmrate"));
                                            float distance = Float.parseFloat(bundle.getString(Constants.Distance));
                                            Price = FlatRate + (Float.parseFloat(String.format("%.2f", distance)) * KMRate);

                                            if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                                                itemCountPrice = Price * 70 / 100;
                                                itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                                                itemCountPrice = itemCountPrice * (2 - 1);
                                                Price = itemCountPrice + Price;
                                                parcel_pickup_delivery_1_price.setText("AU$ " + String.format("%.2f", Price));
                                            } else {
                                                parcel_pickup_delivery_1_price.setText("AU$ " + String.format("%.2f", Price));
                                            }
                                        } else if (i == 1) {
                                            parcel_pickup_delivery_12_time.setText(jsonArray.getJSONObject(1).getString("name"));
                                            FlatRate = Float.parseFloat(jsonArray.getJSONObject(1).getString("flatrate"));
                                            KMRate = Float.parseFloat(jsonArray.getJSONObject(1).getString("kmrate"));
                                            float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                                            Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                                            if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                                                itemCountPrice = Price * 70 / 100;
                                                itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                                                itemCountPrice = itemCountPrice * (2 - 1);
                                                Price = itemCountPrice + Price;
                                                parcel_pickup_delivery_12_price.setText("AU$ " + String.format("%.2f", Price));
                                            } else {
                                                parcel_pickup_delivery_12_price.setText("AU$ " + String.format("%.2f", Price));
                                            }
                                        } else if (i == 2) {
                                            parcel_pickup_delivery_2_time.setText(jsonArray.getJSONObject(2).getString("name"));
                                            FlatRate = Float.parseFloat(jsonArray.getJSONObject(2).getString("flatrate"));
                                            KMRate = Float.parseFloat(jsonArray.getJSONObject(2).getString("kmrate"));
                                            float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                                            Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                                            if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                                                itemCountPrice = Price * 70 / 100;
                                                itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                                                itemCountPrice = itemCountPrice * (2 - 1);
                                                Price = itemCountPrice + Price;
                                                parcel_pickup_delivery_2_price.setText("AU$ " + String.format("%.2f", Price));
                                            } else {
                                                parcel_pickup_delivery_2_price.setText("AU$ " + String.format("%.2f", Price));
                                            }
                                        } else if (i == 3) {
                                            parcel_pickup_delivery_4_time.setText(jsonArray.getJSONObject(3).getString("name"));
                                            FlatRate = Float.parseFloat(jsonArray.getJSONObject(3).getString("flatrate"));
                                            KMRate = Float.parseFloat(jsonArray.getJSONObject(3).getString("kmrate"));
                                            float diatance = Float.parseFloat(bundle.getString(Constants.Distance));
                                            Price = FlatRate + (Float.parseFloat(String.format("%.2f", diatance)) * KMRate);

                                            if (Integer.parseInt(bundle.getString(Constants.ItemCount)) > 1) {
                                                itemCountPrice = Price * 70 / 100;
                                                itemCountPrice = itemCountPrice * (Integer.parseInt(bundle.getString(Constants.ItemCount)) - 1);
                                                itemCountPrice = itemCountPrice * (2 - 1);
                                                Price = itemCountPrice + Price;
                                                parcel_pickup_delivery_4_price.setText("AU$ " + String.format("%.2f", Price));
                                            } else {
                                                parcel_pickup_delivery_4_price.setText("AU$ " + String.format("%.2f", Price));
                                            }
                                        }
                                    }
                                }
                            } else {
                                Log.e(TAG, "GetPriceForDelivery ====== " + jsonObject1);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}