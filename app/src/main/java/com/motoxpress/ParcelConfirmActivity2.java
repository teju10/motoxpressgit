package com.motoxpress;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.FileUploader;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.Utility;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Infograins on 9/9/2016.
 */
public class ParcelConfirmActivity2 extends AppCompatActivity {
    Context mContext;
    CustomEditText pick_upname, pickup_contact, dropup_upname, dropup_contact, short_desc, delivery_instruction,
            noti_email, noti_name;
    ImageView parcel_image1, parcel_image2, parcel_image3;
    int image_value = 0;
    File sourceFile[];
    ArrayList<String> imgPaths;
    String imagepath1 = "", imagepath2 = "", imagepath3 = "";
    Bundle bundle;
    String pickupname, pickupcontact, dropupname, dropupcontact, shortdesc, deliveryinstruction, email, sender_name;

    ProgressHUD progressHUD;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_confirm_delivery_3);
        mContext = this;
        init();
        Bind();
        Listner();
    }

    public void init() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.confirm));
    }

    public void Bind() {
        pick_upname = (CustomEditText) findViewById(R.id.pick_upname);
        pickup_contact = (CustomEditText) findViewById(R.id.pickup_contact);
        dropup_upname = (CustomEditText) findViewById(R.id.dropup_upname);
        dropup_contact = (CustomEditText) findViewById(R.id.dropup_contact);
        short_desc = (CustomEditText) findViewById(R.id.short_desc);
        delivery_instruction = (CustomEditText) findViewById(R.id.delivery_instruction);
        noti_email = (CustomEditText) findViewById(R.id.noti_email);
        noti_name = (CustomEditText) findViewById(R.id.noti_name);

        parcel_image1 = (ImageView) findViewById(R.id.parcel_image1);
        parcel_image2 = (ImageView) findViewById(R.id.parcel_image2);
        parcel_image3 = (ImageView) findViewById(R.id.parcel_image3);

        bundle = new Bundle();
        bundle = getIntent().getExtras();
        imgPaths = new ArrayList<>();

    }

    public void Listner() {

        parcel_image1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                image_value = 1;
                if (imagepath1.equals("")) {
                    selectImage();
                }
            }
        });
        parcel_image2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_value = 2;
                if (imagepath1.equals("")) {
                    System.out.println("imagepath1 " + imagepath1);
                } else {
                    selectImage();
                }

            }
        });

        parcel_image3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                image_value = 3;
                if (imagepath1.equals("") || imagepath2.equals("")) {

                } else {
                    selectImage();
                }
            }
        });


        ((RippleView) findViewById(R.id.contact_ripple_next_parent)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pickupname = pick_upname.getText().toString();
                pickupcontact = pickup_contact.getText().toString();
                dropupname = dropup_upname.getText().toString();
                dropupcontact = dropup_contact.getText().toString();
                shortdesc = short_desc.getText().toString();
                deliveryinstruction = delivery_instruction.getText().toString();
                email = noti_email.getText().toString();
                sender_name = noti_name.getText().toString();

                if (pickupname.equals("")) {
                    pick_upname.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (pickupcontact.equals("")) {
                    pickup_contact.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (dropupname.equals("")) {
                    dropup_upname.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (dropupcontact.equals("")) {
                    dropup_contact.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (sender_name.equals("")) {
                    noti_name.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (email.equals("")) {
                    noti_email.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (!Utility.isValidEmail(email)) {
                    noti_email.setError("email is not valid");

                /*} else if (shortdesc.equals("")) {
                    short_desc.setHintTextColor(getResources().getColor(R.color.colorPrimary));

                } else if (deliveryinstruction.equals("")) {
                    delivery_instruction.setHintTextColor(getResources().getColor(R.color.colorPrimary));
*/
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        if (imgPaths.size() > 0) {
                            sourceFile = new File[imgPaths.size()];
                            for (int i = 0; i < imgPaths.size(); i++) {
                                sourceFile[i] = new File(imgPaths.get(i));
                            }
                        }
                        new SendPickupcontactTask().execute();
                    } else {
                        Utility.ShowToastMessage(mContext, "Please check your Internet Connection");
                    }
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void selectImage() {
        final CharSequence[] options = {"From Camera", "From Gallery", "Close"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Attach your patient's photograph");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("From Camera")) {
                    Uri outputFileUri = getCaptureImageOutputUri();
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    startActivityForResult(intent, 1);
                } else if (options[item].equals("From Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);
                } else if (options[item].equals("Close")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /*FOR IMAGE SELECTION*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // new functions
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1) {
                Uri imageUri = getPickImageResultUri(data);
                beginCrop(imageUri);
            } else if (requestCode == 2) {
                Uri selectedImage = data.getData();
                beginCrop(selectedImage);
            } else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        }
    }

    public Uri getPickImageResultUri(Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ? getCaptureImageOutputUri() : data.getData();
    }

    private Uri getCaptureImageOutputUri() {
        Uri outputFileUri = null;
        File getImage = mContext.getExternalCacheDir();
        if (getImage != null) {
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));
        }
        return outputFileUri;
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(mContext.getCacheDir(), System.currentTimeMillis() + "cropped.jpeg"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == Activity.RESULT_OK) {
            File f = new File(Crop.getOutput(result).getPath());
            Log.e("profile image path", "profile image path ======= " + f);
            if (image_value == 1) {
                String croped_file = "" + Crop.getOutput(result).getPath();

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
                bitmap = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
                bitmap = Utility.rotateImage(bitmap, f);
                imagepath1 = croped_file;
                parcel_image1.setImageBitmap(bitmap);
                parcel_image2.setVisibility(View.VISIBLE);
                imgPaths.add(imagepath1);
            } else if (image_value == 2) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap2 = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
                bitmap2 = Bitmap.createScaledBitmap(bitmap2, 300, 300, true);
                bitmap2 = Utility.rotateImage(bitmap2, f);
                imagepath2 = croped_file;
                parcel_image2.setImageBitmap(bitmap2);
                imagepath2 = croped_file;
                ((ImageView) findViewById(R.id.parcel_image3)).setVisibility(View.VISIBLE);
                imgPaths.add(imagepath2);
            } else if (image_value == 3) {
                String croped_file = "" + Crop.getOutput(result).getPath();
                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap3 = BitmapFactory.decodeFile(f.getAbsolutePath(), bmOptions);
                bitmap3 = Bitmap.createScaledBitmap(bitmap3, 300, 300, true);
                bitmap3 = Utility.rotateImage(bitmap3, f);
                imagepath3 = croped_file;
                parcel_image3.setImageBitmap(bitmap3);
                imagepath3 = croped_file;
                imgPaths.add(imagepath3);
            }
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(mContext, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public class SendPickupcontactTask extends AsyncTask<String, String, String> {

        String result = "";


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressHUD = ProgressHUD.show(mContext, false, true, null);
           /* ((LinearLayout) findViewById(R.id.progress_linear)).setVisibility(View.VISIBLE);
            ((UberProgressView) findViewById(R.id.circularprogress)).isShown();
*/
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                FileUploader multipart = new FileUploader(Constants.SERVER_URL + Constants.INSERT_PARCEL, "UTF-8");
                multipart.addFormField("uid", Utility.getSharedPreferences(mContext, Constants.USERID));
                multipart.addFormField("picupaddress", bundle.getString(Constants.PickUpAdd));
                multipart.addFormField("dropoffaddress", bundle.getString(Constants.DropOffAdd));
                multipart.addFormField("picupcontactname", pickupname);
                multipart.addFormField("dropoffcontactname", dropupname);
                multipart.addFormField("pickupunitnumber", bundle.getString(Constants.PickUpUnit));
                multipart.addFormField("dropoffunitnumber", bundle.getString(Constants.DropOffUnit));
                multipart.addFormField("pickupcnatctnumber", pickupcontact);
                multipart.addFormField("dropoffcontactnumber", dropupcontact);
                multipart.addFormField("picupcompany", bundle.getString(Constants.PickUpCompany));
                multipart.addFormField("dropoffcompany", bundle.getString(Constants.DropOffCompany));
                multipart.addFormField("short_ins", shortdesc);
                multipart.addFormField("delivery_ins", deliveryinstruction);
                multipart.addFormField("noti_email", email);
                multipart.addFormField("total_cost", bundle.getString(Constants.Price));
                System.out.println("SecondPayment Price" +bundle.getString(Constants.Price));
                multipart.addFormField("delivery_plan", bundle.getString(Constants.DeliveryType));
                multipart.addFormField("delivery_time", bundle.getString(Constants.DropOffTime));
                multipart.addFormField("total_distance", bundle.getString(Constants.Distance));
                multipart.addFormField("deliver_by", sender_name);
                multipart.addFormField("pic_at_time", bundle.getString(Constants.PickUpTime));
                multipart.addFormField("parcle_type", bundle.getString(Constants.Type));
                multipart.addFormField("no_of_item", bundle.getString(Constants.ItemCount));
                multipart.addFormField("width", bundle.getString(Constants.Width));
                multipart.addFormField("height", bundle.getString(Constants.Height));
                multipart.addFormField("weight", bundle.getString(Constants.Weight));
                multipart.addFormField("length", bundle.getString(Constants.Length));
                multipart.addFormField("plat", bundle.getString(Constants.PickUpLat));
                multipart.addFormField("plong", bundle.getString(Constants.PickUpLong));
                multipart.addFormField("dlat", bundle.getString(Constants.DropOffLat));
                multipart.addFormField("dlong", bundle.getString(Constants.DropOffLong));
                if (imgPaths.size() != 0) {
                    for (int i = 0; i < imgPaths.size(); i++) {
                        multipart.addFilePart("img", sourceFile[i]);
                    }
                }
                result = multipart.finish();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (progressHUD != null && progressHUD.isShowing()) {
                progressHUD.dismiss();
            }
            if (result == null) {
                Utility.ShowToastMessage(mContext, "server not responding");
            } else {
                try {
                    Log.e("result", "result ====> " + result);
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equalsIgnoreCase("1")) {
                        Utility.ShowToastMessage(mContext, "please pay payment to complete your  parcel delivery");
                        Intent intent = new Intent(mContext, ParcelPaymentActivity.class);
                        intent.putExtras(bundle);
                        intent.putExtra(Constants.ParcelID, json.getString("parcelid"));
                        startActivity(intent);
                        //parcelid
                    } else {
                        Utility.ShowToastMessage(mContext, "Unable to process...");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result);
        }
    }

}