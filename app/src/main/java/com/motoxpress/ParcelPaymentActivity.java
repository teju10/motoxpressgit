package com.motoxpress;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * Created by Infograins on 9/15/2016.
 */
public class ParcelPaymentActivity extends AppCompatActivity {

    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    // note that these credentials will differ between live & sandbox environments.
    private static final String CONFIG_CLIENT_ID = "AW5b74g3115FGGe45Y_VwYdRcfbZlqDGnHfwBMj7gKXdx3NtgLs3WkH7sT7cNHJGF-tUgL3WxVUkdrbo";
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID);
            // use in live mode
            /*.merchantName("MotoXpress")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));*/
    public String TAG = ParcelPaymentActivity.class.getSimpleName();
    Context mContext;
    ResponseTask responseTask;
    Bundle bundle;
    String Parcel_Id = "";
    // The following are only used in PayPalFuturePaymentActivity.
/*            .merchantName("MotoXpress")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));*/
    RelativeLayout payment_paypal, payment_credit_card;
    ProgressHUD progressHUD;
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_payment);
        mContext = this;
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        bundle = new Bundle();
        bundle = getIntent().getExtras();
        Parcel_Id = getIntent().getStringExtra(Constants.ParcelID);
        Utility.setSharedPreference(mContext, Constants.ParcelID, Parcel_Id);
        Utility.setSharedPreference(mContext, Constants.Price, bundle.getString(Constants.Price));
        System.out.println("Payment Price "+bundle.getString(Constants.Price));
        init();

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        payment_paypal = (RelativeLayout) findViewById(R.id.payment_paypal);
        payment_credit_card = (RelativeLayout) findViewById(R.id.payment_credit_card);

        payment_paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PayPayment();
            }
        });

        payment_credit_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, CreditCardActivity.class).putExtra(Constants.ParcelID,Parcel_Id).putExtra("AMOUNT",bundle.getString(Constants.Price)));

            }
        });

        ((RelativeLayout) findViewById(R.id.payment_account)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {
                    // Utility.showToast(mContext,"coming soon");
                    startActivity(new Intent(mContext, AccountActivity.class));
                } else {
                    Utility.ShowToastMessage(mContext, "You need to login first");
                }

            }
        });
    }

    public void init() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.payment));

    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        stopService(new Intent(this, PayPalService.class));
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void PayPayment() {
        PayPalPayment thingToBuy = getThingToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(mContext, PaymentActivity.class);

        // send the same configuration for restart resiliency
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(intent, REQUEST_CODE_PAYMENT);
    }

    private PayPalPayment getThingToBuy(String paymentIntent) {
        return new PayPalPayment(new BigDecimal(bundle.getString(Constants.Price)), "AUD", "Parcel Payment", paymentIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        Log.e(TAG, confirm.toJSONObject().toString(4));
                        Log.e(TAG, confirm.getPayment().toJSONObject().toString(4));
                        ConfirmParcelTask();
                    } catch (JSONException e) {
                        Log.e(TAG, "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.e(TAG, "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.e(TAG, "An invalid Payment or PayPalConfiguration was submitted. Please see the docs.");
            }
        }
    }

    public void ConfirmParcelTask() {
        //https://infograins.com/INFO01/motoxpress/api/api.php?action=UpdateParcelPayment&parcelid=12&payment_method=paypal
        progressHUD = ProgressHUD.show(mContext, true, false, null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("parcelid", Parcel_Id);
            jsonObject.put("payment_method", "paypal");
            jsonObject.put("amount", Utility.getSharedPreferences(mContext,Constants.Price));
            jsonObject.put("user_id", Utility.getSharedPreferences(mContext,Constants.USERID));
            responseTask = new ResponseTask(ParcelPaymentActivity.this, jsonObject, Constants.UPDATE_PARCEL_PAYMENT, TAG, "post");

            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Parcel Send Successfully");
                                Intent intent = new Intent(mContext, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
                                Utility.ShowToastMessage(mContext, "Unable to process...");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            responseTask.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}