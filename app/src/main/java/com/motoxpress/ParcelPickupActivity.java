package com.motoxpress;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andexert.library.RippleView;
import com.motoxpress.customwidget.CustomTextView;

/**
 * Created by Infograins on 9/6/2016.
 */
public class ParcelPickupActivity extends AppCompatActivity {

    Context mContext;
    Toolbar toolbar;
    RippleView ripple_next_parent, ripple_choose_another_time, ripple_choose_another_date;

    LinearLayout parcel_pickup_delivery_main_layout, parcel_pickup_delivery_1h, parcel_pickup_delivery_2_3h,
            parcel_pickup_delivery_4_5h, parcel_pickup_choose_another_time_layout, next_icon,
            parcel_pickup_choose_another_date_layout, parcel_pickup_delivery_time_layout;

    LinearLayout parcel_pickup_delivery_12, parcel_pickup_delivery_1, parcel_pickup_delivery_2, parcel_pickup_delivery_4;

    CustomTextView parcel_pickup_delivery_12_time, parcel_pickup_delivery_1_time, parcel_pickup_delivery_2_time,
            parcel_pickup_delivery_4_time, parcel_pickup_delivery_12_price, parcel_pickup_delivery_1_price,
            parcel_pickup_delivery_2_price, parcel_pickup_delivery_4_price;

    CustomTextView parcel_pickup_distance, parcel_pickup_pickup_time/*, parcel_pickup_delivery_time*/;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_pickup);
        mContext = this;
        init();
        bind();
        listener();
    }

    public void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((TextView) toolbar.findViewById(R.id.toolbar_title)).setText(getResources().getString(R.string.confirm));
        ((CustomTextView) findViewById(R.id.title)).setText(getResources().getString(R.string.summary));
    }

    public void bind() {
        parcel_pickup_distance = (CustomTextView) findViewById(R.id.parcel_pickup_distance);
        parcel_pickup_pickup_time = (CustomTextView) findViewById(R.id.parcel_pickup_pickup_time);
      //  parcel_pickup_delivery_time = (CustomTextView) findViewById(parcel_pickup_delivery_time);

        ripple_next_parent = (RippleView) findViewById(R.id.ripple_next_parent);
        ripple_choose_another_time = (RippleView) findViewById(R.id.ripple_choose_another_time);
        ripple_choose_another_date = (RippleView) findViewById(R.id.ripple_choose_another_date);

        parcel_pickup_delivery_main_layout = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_main_layout);
        parcel_pickup_delivery_1h = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_1h);
        parcel_pickup_delivery_2_3h = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_2_3h);
        parcel_pickup_delivery_4_5h = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_4_5h);
        parcel_pickup_choose_another_time_layout = (LinearLayout) findViewById(R.id.parcel_pickup_choose_another_time_layout);
        next_icon = (LinearLayout) findViewById(R.id.next_icon);
        parcel_pickup_choose_another_date_layout = (LinearLayout) findViewById(R.id.parcel_pickup_choose_another_date_layout);
        parcel_pickup_delivery_time_layout = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_time_layout);

        parcel_pickup_delivery_12 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_12);
        parcel_pickup_delivery_1 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_1);
        parcel_pickup_delivery_2 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_2);
        parcel_pickup_delivery_4 = (LinearLayout) findViewById(R.id.parcel_pickup_delivery_4);

        parcel_pickup_delivery_12_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_12_time);
        parcel_pickup_delivery_1_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_1_time);
        parcel_pickup_delivery_2_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_2_time);
        parcel_pickup_delivery_4_time = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_4_time);
        parcel_pickup_delivery_12_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_12_price);
        parcel_pickup_delivery_1_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_1_price);
        parcel_pickup_delivery_2_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_2_price);
        parcel_pickup_delivery_4_price = (CustomTextView) findViewById(R.id.parcel_pickup_delivery_4_price);
    }

    public void listener() {
        parcel_pickup_delivery_12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }

            }
        });

        parcel_pickup_delivery_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }
            }
        });

        parcel_pickup_delivery_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.colorPrimary));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.white));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.colorPrimary));

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                }
            }
        });

        parcel_pickup_delivery_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                parcel_pickup_delivery_12_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_time.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_time.setTextColor(getResources().getColor(R.color.white));

                parcel_pickup_delivery_12_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_1_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_2_price.setTextColor(getResources().getColor(R.color.colorPrimary));
                parcel_pickup_delivery_4_price.setTextColor(getResources().getColor(R.color.white));

                if(Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    parcel_pickup_delivery_12.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_white));
                    parcel_pickup_delivery_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.rounded_layout_red));
                } else {
                    parcel_pickup_delivery_12.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_1.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_2.setBackground(getResources().getDrawable(R.drawable.rounded_layout_white, getResources().newTheme()));
                    parcel_pickup_delivery_4.setBackground(getResources().getDrawable(R.drawable.rounded_layout_red, getResources().newTheme()));
                }
            }
        });
    }
}