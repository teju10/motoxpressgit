package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Spinner;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.Utility;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Tejsh on 8/26/2016.
 */
public class ParcelTypeActivity extends AppCompatActivity implements View.OnClickListener {

    public String[] cats = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14"};
    Context mContext;
    String type = "document";
    CustomTextView document_txt, send_standard_parcel_txt, parcel_detail_instruction;
    ImageView send_document_red, send_standard_parcel_white;
    Spinner parcel_weight, parcel_lenght, parcel_width, parcel_height;
    RelativeLayout send_doc, send_standard_parcel;
    LinearLayout add_item_layout, bottom_layout, weight_boxes_layout;
    AlertDialog alertDialog;
    int cat = -1;
    String quantityitem;
    double distanceADouble = 0.0;
    Bundle bundle;
    String[] weight, height, width, length;
    ArrayList<HashMap<String, String>> weight_spinner, length_spinner, height_spinner, width_spinner;
    String WeightString = "", HeightString = "", WidthString = "", LenghtString = "";

    ProgressHUD progresshud;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_detail);
        mContext = this;
        Find();
        Listner();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("PARCEL DETAILS");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((CustomTextView) findViewById(R.id.title)).setText(getResources().getString(R.string.parcel_type));
    }

    public void Find() {
        document_txt = (CustomTextView) findViewById(R.id.document_txt);
        send_standard_parcel_txt = (CustomTextView) findViewById(R.id.send_standard_parcel_txt);
        parcel_detail_instruction = (CustomTextView) findViewById(R.id.parcel_detail_instruction);

        send_document_red = (ImageView) findViewById(R.id.send_document_red);
        send_standard_parcel_white = (ImageView) findViewById(R.id.send_standard_parcel_white);

        parcel_weight = (Spinner) findViewById(R.id.parcel_weight);
        parcel_lenght = (Spinner) findViewById(R.id.parcel_lenght);
        parcel_width = (Spinner) findViewById(R.id.parcel_width);
        parcel_height = (Spinner) findViewById(R.id.parcel_height);

        send_doc = (RelativeLayout) findViewById(R.id.send_doc);
        send_standard_parcel = (RelativeLayout) findViewById(R.id.send_standard_parcel);
        add_item_layout = (LinearLayout) findViewById(R.id.add_item_layout);
        bottom_layout = (LinearLayout) findViewById(R.id.bottom_layout);
        weight_boxes_layout = (LinearLayout) findViewById(R.id.weight_boxes_layout);

        /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            send_standard_parcel.setBackgroundDrawable(getResources().getDrawable(R.drawable.center_send_parcel_grey_bg));
        } else {
            send_standard_parcel.setBackground(getResources().getDrawable(R.drawable.center_send_parcel_grey_bg, getResources().newTheme()));
        }*/
    }

    public void Listner() {
        ChangeAllColor(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.white), R.id.send_doc);
        send_doc.setOnClickListener(this);
        send_standard_parcel.setOnClickListener(this);

        weight = mContext.getResources().getStringArray(R.array.sendparcel_weight);
        height = mContext.getResources().getStringArray(R.array.sendparcel_size);
        width = mContext.getResources().getStringArray(R.array.sendparcel_size);
        length = mContext.getResources().getStringArray(R.array.sendparcel_size);


        setweight_spinner();
        setheight_spinner();
        setlength_spinner();
        setwidth_spinner();

        /*FOR GET BUNDLE FROM PREVIOUS CLASS*/

        findViewById(R.id.add_quantity).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddQuantityDialog();
            }
        });

        findViewById(R.id.calculate_quote).setOnClickListener(this);
    }

    public void calculateQuote() {
        bundle = new Bundle();
        bundle = getIntent().getExtras();
       // Utility.setSharedPreference(mContext, Constants.TYPE);
        if (((CustomTextView) findViewById(R.id.no_of_items)).getText().toString().equals("")) {
            Utility.ShowToastMessage(mContext, "please add item first");
        } else if (type.equalsIgnoreCase(Constants.Standard)) {
            if (WeightString.equals("(required)")) {
                ((CustomTextView) findViewById(R.id.error_weight_text)).
                        setTextColor(getResources().getColor(R.color.colorPrimary));
            } else if (LenghtString.equals("(required)")) {
                ((CustomTextView) findViewById(R.id.error_length_text)).
                        setTextColor(getResources().getColor(R.color.colorPrimary));
            } else if (WidthString.equals("(required)")) {
                ((CustomTextView) findViewById(R.id.error_width_text)).
                        setTextColor(getResources().getColor(R.color.colorPrimary));
            } else if (HeightString.equals("(required)")) {
                ((CustomTextView) findViewById(R.id.error_height_text)).
                        setTextColor(getResources().getColor(R.color.colorPrimary));

            } else if (type.equalsIgnoreCase(Constants.Standard)) {
                bundle.putString(Constants.ItemCount, ((CustomTextView) findViewById(R.id.no_of_items)).getText().toString());
                bundle.putString(Constants.Width, WidthString.replaceAll("cm",""));
                bundle.putString(Constants.Height, HeightString.replaceAll("cm",""));
                bundle.putString(Constants.Length, LenghtString.replaceAll("cm",""));
                bundle.putString(Constants.Weight, WeightString.replaceAll("kg",""));
                 bundle.putString(Constants.Type, type);
                Intent intent = new Intent(mContext, ParcelConfirmActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        } else {
            bundle.putString(Constants.ItemCount, ((CustomTextView) findViewById(R.id.no_of_items)).getText().toString());
            bundle.putString(Constants.Type, type);
            Intent intent = new Intent(mContext, ParcelConfirmActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    public void setweight_spinner() {
        weight_spinner = new ArrayList<>();
        for (int i = 0; i < weight.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", weight[i]);
            weight_spinner.add(hm);
        }
        int[] to = new int[]{R.id.p_detail};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, weight_spinner, R.layout.text_single_add, from, to);
        parcel_weight.setAdapter(adapter);
        parcel_weight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WeightString = weight_spinner.get(position).get("Type").toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setheight_spinner() {
        height_spinner = new ArrayList<>();
        for (int i = 0; i < height.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", height[i]);
            height_spinner.add(hm);
        }
        int[] to = new int[]{R.id.p_detail};
        String[] from = new String[]{"Type"};

        SimpleAdapter adapter = new SimpleAdapter(mContext, height_spinner, R.layout.text_single_add, from, to);
        parcel_height.setAdapter(adapter);
        parcel_height.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                HeightString = height_spinner.get(position).get("Type").toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setlength_spinner() {
        length_spinner = new ArrayList<>();
        for (int i = 0; i < length.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", length[i]);
            length_spinner.add(hm);
        }
        int[] to = new int[]{R.id.p_detail};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, length_spinner, R.layout.text_single_add, from, to);
        parcel_lenght.setAdapter(adapter);
        parcel_lenght.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                LenghtString = length_spinner.get(position).get("Type").toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void setwidth_spinner() {
        width_spinner = new ArrayList<>();
        for (int i = 0; i < width.length; i++) {
            HashMap<String, String> hm = new HashMap<>();
            hm.put("Type", width[i]);
            width_spinner.add(hm);
        }
        int[] to = new int[]{R.id.p_detail};
        String[] from = new String[]{"Type"};
        // Creating a SimpleAdapter for the Spinner
        SimpleAdapter adapter = new SimpleAdapter(mContext, width_spinner, R.layout.text_single_add, from, to);
        parcel_width.setAdapter(adapter);
        parcel_width.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                WidthString = width_spinner.get(position).get("Type").toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void AddQuantityDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View convertView = li.inflate(R.layout.dialog_category, null);
        alertDialogBuilder.setView(convertView);
        alertDialogBuilder.setCancelable(false);
        alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        ((ListView) convertView.findViewById(R.id.list)).setAdapter(new SingleListAdapter(mContext, cats));
        if (cat != -1) {
            ((ListView) convertView.findViewById(R.id.list)).setItemChecked(cat, true);
        }
        (convertView.findViewById(R.id.txtcancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        (convertView.findViewById(R.id.txtdone)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = ((ListView) convertView.findViewById(R.id.list)).getCheckedItemPosition();
                if (i != -1) {
                    cat = i;
                }
                ((CustomTextView) findViewById(R.id.no_of_items)).setText(quantityitem);
                alertDialog.cancel();
            }
        });

        ((ListView) convertView.findViewById(R.id.list)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                quantityitem = (String) adapterView.getItemAtPosition(position);
                System.out.println("itemposition " + quantityitem);
                System.out.println("DONE");

            }
        });

        alertDialog.show();
    }

    public void ChangeAllColor(@ColorInt int selectedcolor, @ColorInt int unselectedcolor, int ids) {
        switch (ids) {
            case R.id.send_doc:

                send_doc.setBackgroundDrawable(getDrawablemy(R.drawable.left_send_parcel_grey_bg, selectedcolor));
                send_document_red.setBackgroundDrawable(getResources().getDrawable(R.drawable.document));
                document_txt.setTextColor(unselectedcolor);
                send_standard_parcel.setBackgroundColor(unselectedcolor);
                send_standard_parcel_white.setBackgroundDrawable(getResources().getDrawable(R.drawable.standard_parsal_red));
                send_standard_parcel_txt.setTextColor(selectedcolor);
                send_standard_parcel.setBackgroundDrawable(getDrawablemy(R.drawable.left_send_parcel_grey_bg, unselectedcolor));

                parcel_detail_instruction.setVisibility(View.VISIBLE);
                parcel_detail_instruction.setText(getResources().getString(R.string.select_standardparcel));
                bottom_layout.setVisibility(View.GONE);
                weight_boxes_layout.setVisibility(View.GONE);
                add_item_layout.setVisibility(View.VISIBLE);
                break;

            case R.id.send_standard_parcel:
                send_document_red.setBackgroundDrawable(getResources().getDrawable(R.drawable.document_red));
                document_txt.setTextColor(selectedcolor);
                send_standard_parcel.setBackgroundColor(selectedcolor);
                send_standard_parcel_white.setBackgroundDrawable(getResources().getDrawable(R.drawable.standard_parsal));
                send_standard_parcel_txt.setTextColor(unselectedcolor);
                send_doc.setBackgroundDrawable(getDrawablemy(R.drawable.left_send_parcel_grey_bg, unselectedcolor));

                parcel_detail_instruction.setVisibility(View.INVISIBLE);
                bottom_layout.setVisibility(View.VISIBLE);
                weight_boxes_layout.setVisibility(View.VISIBLE);
                add_item_layout.setVisibility(View.VISIBLE);

                break;
        }
    }

    public GradientDrawable getDrawablemy(@DrawableRes int draw, @ColorInt int color) {
        GradientDrawable footerBackground = (GradientDrawable) getResources().getDrawable(draw);
        footerBackground.setColor(color);
        return footerBackground;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.send_doc:
                type = "document";
                ChangeAllColor(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.white), R.id.send_doc);
                break;
            case R.id.send_standard_parcel:
                type = "standard";
                ChangeAllColor(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.white), R.id.send_standard_parcel);
                break;
            case R.id.calculate_quote:
                calculateQuote();
                break;
        }
    }

    public class SingleListAdapter extends BaseAdapter {
        Context ctx;
        LayoutInflater lInflater;
        String[] data;

        public SingleListAdapter(Context context, String[] data) {
            ctx = context;
            this.data = data;
            lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.length;
        }

        @Override
        public Object getItem(int position) {
            return data[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = lInflater.inflate(R.layout.single_choice_items, parent, false);
            }
            ((CustomTextView) view.findViewById(R.id.singleitemId)).setText(data[position]);
            return view;
        }
    }
}