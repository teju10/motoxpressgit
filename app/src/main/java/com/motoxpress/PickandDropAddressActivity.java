package com.motoxpress;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.motoxpress.customwidget.CustomAutoCompleteTextView;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.GPSTracker;
import com.motoxpress.utility.PlaceJSONParser;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Tejsh on 8/25/2016.
 */
public class PickandDropAddressActivity extends AppCompatActivity {

    Context mContext;
    View rootView;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    CustomAutoCompleteTextView pickup_address;
    ParserTaskforautotv parserTask;
    String pickupaddress = "", dropaddress;
    ListView list;
    GPSTracker gpsTracker;
    LocationManager locationManager;
    double latitude = 0.0, longitude = 0.0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickanddrop_address);

        mContext = this;
        share = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("FROM");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(mContext);
        }
        gpsTracker = new GPSTracker(mContext);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();

        if (latitude == 0 || longitude == 0) {

        }

        Find();
        Initialize();
    }

    public void Find() {
        pickup_address = (CustomAutoCompleteTextView) findViewById(R.id.pickup_address);
        list = (ListView) findViewById(R.id.list);
    }

    public void Initialize() {
        /*FOR AUTOCOMPLETE METHOD*/

        pickup_address.setThreshold(1);
        pickup_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int i, int i1, int i2) {
                Log.e("onTextChanged", "CharSequence ===" + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                pickupaddress = "";

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((CardView) findViewById(R.id.listcardview)).setVisibility(View.VISIBLE);
                PlacesTask placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }
        });
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED);
        finish();//finishing activity
    }

    private class PlacesTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... place) {
            String data = "";
            try{
            String key = "key=AIzaSyC-gqFmMLlPW-CCV0no5OyuX6HP9Cv5smg";
            String input = "";
            try {
                input = "input=" + URLEncoder.encode(place[0], "utf-8");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            // https://maps.googleapis.com/maps/api/place/autocomplete/xml?input=Amoeba&types=establishment&
            // location=37.76999,-122.44696&radius=500&key=YOUR_API_KEY
            String location = "location="+String.valueOf(latitude) + "," + String.valueOf(longitude);
            String radius = "radius=50000";
            String types = "types=geocode";
            String sensor = "sensor=true";
            String parameters = input + "&" + types + "&" + location + "&" + radius + "&" + sensor + "&" + key;
            String output = "json";
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/" + output + "?" + parameters;
            try {
                Log.e("downloadUrl", "url ==== " + url);
                data = downloadUrl(url);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }catch (Exception e){
            e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            parserTask = new ParserTaskforautotv();
            parserTask.execute(result);
        }
    }

    private class ParserTaskforautotv extends AsyncTask<String, Integer, List<HashMap<String, String>>> {
        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {
            List<HashMap<String, String>> places = null;
            PlaceJSONParser placeJsonParser = new PlaceJSONParser();
            try {
                jObject = new JSONObject(jsonData[0]);
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);
            } catch (Exception e) {
                Log.d("Exception", e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {
            String[] from = new String[]{"description"};
            try {
                int[] to = new int[]{android.R.id.text1};
                // Creating a SimpleAdapter for the AutoCompleteTextView

                SimpleAdapter adapter = new SimpleAdapter(mContext, result, android.R.layout.simple_list_item_1, from, to);
                // Setting the adapter
                adapter.notifyDataSetChanged();
                list.setAdapter(adapter);
                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        pickupaddress = ((HashMap<String, String>) parent.getItemAtPosition(position)).get("description");
                        dropaddress = ((HashMap<String, String>) parent.getItemAtPosition(position)).get("description");
                        Intent intent = new Intent();
                        intent.putExtra("PICKADDRESS", pickupaddress);
                        intent.putExtra("DROPADDRESS", dropaddress);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}