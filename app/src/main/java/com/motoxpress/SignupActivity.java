package com.motoxpress;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.GPSTracker;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

/**
 * Created by Tejsh on 8/17/2016.
 */
public class SignupActivity extends AppCompatActivity {


    public String TAG = SignupActivity.class.getSimpleName();
    Context mContext;
    double latitude = 0.0, longitude = 0.0;
    LocationManager locationManager;
    GPSTracker gpsTracker;
    ProgressHUD progresshud;
    CustomEditText signup_fullname, signup_phoneno, signup_email, signup_password, signup_repassword;

    String emailString = "", passwordString = "", confirmpasswordString = "", firstNameString = "",
            strsignup_phoneno = "", strsignup_verifaction = "";

    SharedPreferences share;
    SharedPreferences.Editor edit;
    CustomEditText commet_review;
    ResponseTask responseTask;
    Dialog dialog_verification;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mContext = this;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((CustomTextView) findViewById(R.id.toolbar_title)).setText("REGISTER");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        init();
        bind();
        Listeners();
    }

    @Override
    protected void onResume() {
        gpsTracker = new GPSTracker(mContext);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        super.onResume();
    }

    public void init() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            Utility.AlertNoGPS(mContext);
        }
        gpsTracker = new GPSTracker(mContext);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        /*if (Utility.getSharedPreferences(mContext, "GCMID").equals("")) {
            GCMIntentService.registerForNotification(mContext);
        }*/
        share = mContext.getSharedPreferences("pref", MODE_PRIVATE);
    }

    public void bind() {
        signup_fullname = (CustomEditText) findViewById(R.id.signup_fullname);
        signup_phoneno = (CustomEditText) findViewById(R.id.signup_phoneno);
        signup_email = (CustomEditText) findViewById(R.id.signup_email);
        signup_password = (CustomEditText) findViewById(R.id.signup_password);
        signup_repassword = (CustomEditText) findViewById(R.id.signup_repassword);
    }

    public void Listeners() {
        ((CustomButton) findViewById(R.id.signup_signupbtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firstNameString = signup_fullname.getText().toString();
                strsignup_phoneno = signup_phoneno.getText().toString();
                emailString = signup_email.getText().toString();
                passwordString = signup_password.getText().toString();
                confirmpasswordString = signup_repassword.getText().toString();

                if (firstNameString.equals("")) {
                    signup_fullname.setHintTextColor(getResources().getColor(R.color.red));
                } else if (strsignup_phoneno.equals("")) {
                    signup_phoneno.setHintTextColor(getResources().getColor(R.color.red));
                } else if (emailString.equals("")) {
                    signup_email.setHintTextColor(getResources().getColor(R.color.red));
                } else if (!Utility.isValidEmail(emailString)) {
                    signup_email.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "email not valid");
                } else if (passwordString.equals("")) {
                    signup_password.setHintTextColor(getResources().getColor(R.color.red));
                } else if (passwordString.length() <= 5) {
                    signup_password.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "Password length contain atleast 6 digit!!!");
                } else if (!passwordString.equals(confirmpasswordString)) {
                    signup_repassword.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "password not matched");
                } else if (latitude == 0 || longitude == 0) {
                    Utility.AlertNoGPS(mContext);
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        //Utility.ShowToastMessage(mContext, "coming soons");
                        SignUpTask();
                    } else {
                        Utility.ShowToastMessage(mContext, "Check your internet connection");
                    }
                }
            }

        });
    }

    public void SignUpTask() {
        progresshud = ProgressHUD.show(mContext, true,
                false, null);

/*http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/motoxpress/api/api.php?action=Register&
fullname=abdul&mobile=7389360332&email=gdfgdfg.infograins@gmail.com&password=123456&device_type=17747&
deviceid=74747&lat=4747&long=74747.00&device_token=123*/
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fullname", firstNameString);
            jsonObject.put("email", emailString);
            jsonObject.put("mobile", strsignup_phoneno);
            jsonObject.put("password", passwordString);
            jsonObject.put("device_type", "android");
            jsonObject.put("deviceid", Utility.getSharedPreferences(mContext, Constants.FCMID));
            jsonObject.put("lat", String.valueOf(latitude));
            jsonObject.put("long", String.valueOf(longitude));
            jsonObject.put("device_token", Utility.getSharedPreferences(mContext, Constants.IMEINUMBER));
            responseTask = new ResponseTask(mContext, jsonObject, Constants.REGISTER, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else if (result.equalsIgnoreCase("SocketTimeoutException")) {
                        Utility.ShowToastMessage(mContext, "Server time out");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {

                                JSONObject jsonObject1 = json.getJSONObject("userdetails");
                                Utility.setSharedPreference(mContext, Constants.SPLOGIN, "1");
                                Utility.setSharedPreference(mContext, Constants.USERID, jsonObject1.getString("userid"));
                                Utility.setSharedPreference(mContext, Constants.EMAIL, jsonObject1.getString("email"));
                                Alert("Account Activation", "please check your email for verification code");
                               /* startActivity(new Intent(mContext, LoginActivity.class));
                                finish();*/
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void Alert(final String title, final String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(message).setCancelable(false).setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        verificationAlert();
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void verificationAlert() {
        dialog_verification = new Dialog(mContext);
        dialog_verification.setCancelable(false);
        dialog_verification.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_verification.setContentView(R.layout.dialog_varification);
        dialog_verification.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        commet_review = (CustomEditText) dialog_verification.findViewById(R.id.commet_review);

        (dialog_verification.findViewById(R.id.skip_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_verification.dismiss();
                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        (dialog_verification.findViewById(R.id.review_submit)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(commet_review.getText().toString().equals("")){
                    Utility.ShowToastMessage(mContext, "please enter verification code");
                } else {
                    if (Utility.isConnectingToInternet(mContext)) {
                        VerificationTask(commet_review.getText().toString());
                        dialog_verification.dismiss();
                    } else {
                        Utility.ShowToastMessage(mContext, "Check your internet connection");
                    }
                }
            }
        });
        dialog_verification.show();
    }

    public void VerificationTask(String code){
        progresshud = ProgressHUD.show(mContext, true,
                false, null);
//https://infograins.com/INFO01/motoxpress/api/api.php?action=Register&fullname=raj&mobile=12&
// email=fsdfhjsdhffkdf&password=56hh&device_type=656&deviceid=877&lat=14&long=4545
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("code", code);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.VerifyAccount, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progresshud != null && progresshud.isShowing()) {
                        progresshud.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Verification Completed please login");
                                startActivity(new Intent(mContext, LoginActivity.class));
                                finish();
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}