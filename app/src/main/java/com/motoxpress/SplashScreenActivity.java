package com.motoxpress;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.motoxpress.constants.Constants;
import com.motoxpress.utility.GPSTracker;
import com.motoxpress.utility.Utility;

/**
 * Created by Tejsh on 8/16/2016.
 */

public class SplashScreenActivity extends AppCompatActivity {
    Context mContext;
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);
        mContext = this;
        gpsTracker = new GPSTracker(mContext);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String IMEINumber = telephonyManager.getDeviceId();
        Utility.setSharedPreference(mContext, Constants.IMEINUMBER, IMEINumber);
        if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            String token = FirebaseInstanceId.getInstance().getToken();
            System.out.println("token is" + token);
            Utility.setSharedPreference(getApplicationContext(), Constants.FCMID, token);
        }
        if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {

                    Intent i = new Intent(mContext, HomeActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
                }

            }, 2000);
        } else {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent i = new Intent(mContext, MainActivity.class);
                    startActivity(i);
                    finish();
                    overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

                }
            }, 2000);
        }
    }
}
