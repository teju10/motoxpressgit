package com.motoxpress.constants;

/**
 * Created by Tejsh on 07-Jul-16.
 */
public class Constants {

    /*  development SHA key for google
    keytool -list -v -keystore "C:\Users\Tejush\.android\debug.keystore" -alias androiddebugkey -storepass android -keypass android
    */
/* development hash key for facebook
keytool -exportcert -alias androiddebugkey -keystore "C:\Users\Tejush\.android\debug.keystore" | "C:\Program Files (x86)\GnuWin32\bin\openssl" sha1 -binary |"C:\Program Files (x86)\GnuWin32\bin\openssl" base64
*/
  /*  public static final String PLACES_KEY = "AIzaSyBiL3ar8sOLeQsv_JCKDfGcAFuPwUH9smQ";
    public static final String OAuthKeyGooglePlus = "868025213390-hcc515nuckiqml6p99ooepqva71ovist.apps.googleusercontent.com";
    public static final String FacebookKey = "1759247641026698";*/

    public static final String SERVER_URL = "http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/motoxpress/api/api.php?action=";

   // public static final String SERVER_URL = "https://infograins.com/INFO01/motoxpress/api/api.php?action=";


    /* API Actions */
    public static final String GUEST_REGISTER = "GuestRegister";
    public static final String GET_DELIVERY_TYPE = "GetDeliveryType";
    public static final String INSERT_PARCEL = "InsertParcel";
    public static final String UPDATE_PARCEL_PAYMENT = "UpdateParcelPayment";
    public static final String REGISTER = "Register";
    public static final String FORGOT = "ForgotPassword";
    public static final String VerifyAccount = "VerifyAccount";
    public static final String LOGIN = "Login";
    public static final String UPDATEUSERPROFILE = "updateuserprofile";
    public static final String CHANGEPASSWORD = "ChangePassword";
    public static final String GetUserProfile = "GetUserProfile";
    public static final String PARCELLIST = "ParcelList";
    public static final String LOGOUT = "LogoutUser";
    public static final String PARCELDRIVERSTATUS = "ParcelDriverStatus";
    public static final String NOTIFICATIONLIST = "NotificationList";
    public static final String BILLINGLIST = "BillingList";
    public static final String NOTIFICATIONSTATUS = "NotificationStatus";
    public static final String USERFEEDBACK = "UserFeedback";
    public static final String DRIVERID = "driverid";
    public static final String UpdateParcelPaymentStripe = "UpdateParcelPaymentStripe";



    /* User Keys */
    public static final String SPLOGIN = "SPLOGIN";
    public static final String FCMID = "FCM_ID";

    public static final String USERID = "userid";
    public static final String USER_TYPE = "usertype";
    public static final String PickUpAdd = "PickUpAdd";
    public static final String DropOffAdd = "DropOffAdd";
    public static final String PickUpLat = "PickUpLat";
    public static final String PickUpLong = "PickUpLong";
    public static final String DropOffLat = "DropOffLat";
    public static final String DropOffLong = "DropOffLong";
    public static final String PickUpCompany = "PickUpCompany";
    public static final String DropOffCompany = "DropOffCompany";
    public static final String PickUpUnit = "PickUpUnit";
    public static final String DropOffUnit = "DropOffUnit";
    public static final String ItemCount = "ItemCount";
    public static final String Width = "Width";
    public static final String Height = "Height";
    public static final String Length = "Length";
    public static final String Weight = "Weight";
    public static final String Document = "Document";
    public static final String Standard = "Standard";
    public static final String Other = "Other";
    public static final String Type = "Type";
    public static final String Distance = "Distance";
    public static final String PickUpTime = "PickUpTime";
    public static final String DropOffTime = "DropOffTime";
    public static final String Price = "Price";
    public static final String DeliveryType = "DeliveryType";
    public static final String ParcelID = "ParcelID";
    public static final String APPLYFORACCOUNT = "Applyforaccount";
    public static final String ACCOUNTPAYMENT = "Accountpayment";
    public static final String CARDDETAILSAVE = "CardSave";




    /*SIGNUP*/
    public static final String FULLNAME = "fullname";
    public static final String EMAIL = "email";
    public static final String MOBILE = "mobile";
    public static final String PASSWORD = "password";
    public static final String DEVICETYPE = "device_type";
    public static final String DEVICEID = "deviceid";
    public static final String USER_NAME = "username";
    public static final String USER_EMAIL = "useremail";
    public static final String USER_MOBILE = "usermobile";

    /*FRAGMENT TRANSIT NAME*/
    public static final String PICKANDDROPADDRESSFRAGMENT = "PickandDropAddressFragment";
    public static final String SENDPARCELFRAGMENT = "SendParcelFragment";
    public static final String NOTIFICATIONDETAILFRAGMENT = "NotificationdetailFragment";
    public static final String ACCOUNTDETAIL_CUSTOMERTRANSACTIONFRAGMENT = "AccountDetail_CustomerTransactonFragment";
    public static final String ACCOUNTFRAGMENT = "AccountActivity";
    public static final String MYDELIVERYFRAGMENT = "MyDeliveriesFragment";


    /*OTHER*/
    public static final String PICKUPADDRESS = "pickupaddress";
    public static final String DOCUMENTPARCELCLICK = "DOCUMENTPARCELCLICK";
    public static final String STANDARDPARCELCLICK = "STANDARDPARCELCLICK";
    public static final String OTHERPARCELCLICK = "OTHERPARCELCLICK";
    public static final String QUNATITYITEM = "quantityitem";
    public static final String TRIPDISTANCE = "tripdistance";
    public static final String IMEINUMBER = "imeinumber";
    public static final String NOTIFICATION_STATUS = "notificationstatus";
    public static final String READUNREADSTATUD = "read_unread_status";



}