package com.motoxpress.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.motoxpress.Aboutus_Webview_Activity;
import com.motoxpress.FAQ_Webview_Activity;
import com.motoxpress.PrivacyPolicy_Webview_Activity;
import com.motoxpress.R;
import com.motoxpress.TermsandCondition_Activity;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.utility.Utility;

/**
 * Created by Tejsh on 9/23/2016.
 */
public class AboutUsFragment extends Fragment implements View.OnClickListener {

    View rootView;
    Context mContext;
    CustomButton aboutus,faq,privacy_policy,terms_and_conditions,support,franchise;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_aboutus, container, false);
        mContext = getActivity();
        // share = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);

        Find();
        Listner();
        return rootView;
    }

    public void Find() {
        aboutus = (CustomButton) rootView.findViewById(R.id.aboutus);
        privacy_policy = (CustomButton) rootView.findViewById(R.id.privacy_policy);
        faq = (CustomButton) rootView.findViewById(R.id.faq);
        terms_and_conditions = (CustomButton) rootView.findViewById(R.id.terms_and_conditions);
        support = (CustomButton) rootView.findViewById(R.id.support);
        franchise = (CustomButton) rootView.findViewById(R.id.franchise);
    }

    public void Listner() {
        aboutus.setOnClickListener(this);
        privacy_policy.setOnClickListener(this);
        faq.setOnClickListener(this);
        terms_and_conditions.setOnClickListener(this);
        support.setOnClickListener(this);
        franchise.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.aboutus:
                Intent i = new Intent(mContext, Aboutus_Webview_Activity.class);
                startActivity(i);
                break;

            case R.id.faq:
                Intent faq = new Intent(mContext, FAQ_Webview_Activity.class);
                startActivity(faq);
                break;

            case R.id.privacy_policy:

                Intent p = new Intent(mContext, PrivacyPolicy_Webview_Activity.class);
                startActivity(p);
                break;

         case R.id.terms_and_conditions:
             //Utility.showToast(mContext,"coming soon");
                Intent tc = new Intent(mContext, TermsandCondition_Activity.class);
                startActivity(tc);
                break;

            case R.id.franchise:
                Utility.Alert(mContext, "FRANCHISING","franchising@motoxpress.com.au");
                break;

         case R.id.support:
             Utility.Alert(mContext, "SUPPORT","support@motoxpress.com.au");
            /*    Intent p = new Intent(mContext, PrivacyPolicy_Webview_Activity.class);
                startActivity(p);*/
                break;

        }
    }
}
