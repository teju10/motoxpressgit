package com.motoxpress.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejsh Dani on 19-Aug-16.
 */
public class BillingFragment extends Fragment {

    View rootView;
    Context mContext;
    AdapterBillingList adapter_billinglist;
    ProgressHUD progressHUD;
    ResponseTask responseTask;
    ArrayList<JSONObject> billing_arraylist;
    String TAG = BillingFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_my_billinglist, container, false);
        mContext = getActivity();

        Find();

        return rootView;
    }

    public void Find() {
        if (Utility.isConnectingToInternet(mContext)) {
            BillingList();
        } else {
            Utility.showToast(mContext, "please check your network connection");
        }

    }

    public void BillingList() {
        try {
            progressHUD = ProgressHUD.show(mContext, true, false, null);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            responseTask = new ResponseTask(mContext, jsonObject, Constants.BILLINGLIST, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {

                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                billing_arraylist = new ArrayList<>();
                                JSONArray userdetails = json.getJSONArray("Detail");
                                for (int i = 0; i < userdetails.length(); i++) {
                                    JSONObject jo = null;
                                    jo = userdetails.getJSONObject(i);
                                    billing_arraylist.add(jo);
                                }

                                adapter_billinglist = new AdapterBillingList(mContext, R.layout.adapter_billing_list, billing_arraylist);
                                ((ListView) rootView.findViewById(R.id.my_billinglist)).setAdapter(adapter_billinglist);
                            } else {
                                if (json.getString("msg").equals("No data Found")) {
                                   /* ((ListView) rootView.findViewById(R.id.my_billinglist)).setVisibility(View.GONE);
                                    ((ShimmerTextView) rootView.findViewById(R.id.notitext)).setVisibility(View.VISIBLE);*/
                                    Utility.ShowToastMessage(mContext, "There is no any billing list");

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class AdapterBillingList extends BaseAdapter {

        ArrayList<JSONObject> obj;
        Context mContext;
        int res;


        public AdapterBillingList(Context context, int resource, ArrayList<JSONObject> object) {

            this.mContext = context;
            this.res = resource;
            this.obj = object;
        }

        @Override
        public int getCount() {
            return obj.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            try {
                JSONObject j = obj.get(position);
                viewHolder.billing_parcelid.setText("# Parcel ID : " + j.getString("parcelid"));
                viewHolder.billing_paymenttype.setText(j.getString("payment_method"));
                viewHolder.billing_amount.setText("$ " + j.getString("amount"));
                viewHolder.billing_date.setText("Date : " + j.getString("createdate"));

            } catch (Exception e) {
                e.printStackTrace();
            }

            return convertView;
        }

        class ViewHolder {
            CustomTextView billing_parcelid, billing_paymenttype, billing_amount, billing_date;

            public ViewHolder(View base) {
                billing_parcelid = (CustomTextView) base.findViewById(R.id.billing_parcelid);
                billing_paymenttype = (CustomTextView) base.findViewById(R.id.billing_paymenttype);
                billing_amount = (CustomTextView) base.findViewById(R.id.billing_amount);
                billing_date = (CustomTextView) base.findViewById(R.id.billing_date);

            }

        }

    }

}
