package com.motoxpress.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.motoxpress.HomeActivity;
import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomButton;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;

import eu.erikw.PullToRefreshListView;

/**
 * Created by Tejsh on 19-Aug-16.
 */
public class MyDeliveriesFragment extends Fragment {

    public String TAG = MyDeliveriesFragment.class.getSimpleName();
    View rootView;
    Context mContext;
    ResponseTask responseTask;
    LinkedList<JSONObject> deliveryarraylist = new LinkedList<>();
    ProgressHUD progressHUD;
    ShimmerTextView shimmertv;
    Shimmer shimmer;
    int index = 0;
    private PullToRefreshListView recyclerView;
    private AdapterDeliverylist adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_mydelivery, container, false);
        mContext = getActivity();

        Find();
        Listner();
        return rootView;
    }

    public void Find() {
        recyclerView = (PullToRefreshListView) rootView.findViewById(R.id.mydelivery_list);
        shimmertv = (ShimmerTextView) rootView.findViewById(R.id.shimmer_tv);
        toggleAnimation();
    }

    public void Listner() {
        adapter = new AdapterDeliverylist(mContext, R.layout.adapter_mydelivery, deliveryarraylist);
        recyclerView.setAdapter(adapter);
        MyDeliveyListTask();

        ((RippleView) rootView.findViewById(R.id.send_parcel_ripple)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).displayView(0);
            }
        });
        recyclerView.setOnRefreshListener(new PullToRefreshListView.OnRefreshListener() {

            @Override
            public void onRefresh() {

                MyDeliveyListTask();

                recyclerView.postDelayed(new Runnable() {


                    @Override
                    public void run() {
                        recyclerView.onRefreshComplete();
                    }
                }, 2000);
            }
        });

    }

    public void toggleAnimation() {
        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(shimmertv);
        }
    }

    public void MyDeliveyListTask() {
/*https://infograins.com/INFO01/motoxpress/api/api.php?action=ParcelList&runnerid=1&delivery_status=all*/

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("delivery_status", "all");
            jsonObject.put("page", "" + index);
            if (index == 0) {
                progressHUD = ProgressHUD.show(mContext, true, false, null);
            }
            responseTask = new ResponseTask(mContext, jsonObject, Constants.PARCELLIST, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (progressHUD != null && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {

                                JSONArray userdetails = json.getJSONArray("parcellist");
                                for (int i = userdetails.length(); i > 0; i--) {
                                    JSONObject jo = null;
                                    jo = userdetails.getJSONObject(i - 1);
                                    deliveryarraylist.addFirst(jo);
                                }
                                adapter.loadData();
                            } else {
                                if (json.getString("msg").equals("Job is not available yet")) {
                                    if (index == 0) {
                                        ((ListView) rootView.findViewById(R.id.mydelivery_list)).setVisibility(View.GONE);
                                        ((ShimmerTextView) rootView.findViewById(R.id.shimmer_tv)).setVisibility(View.VISIBLE);
                                        ((RippleView) rootView.findViewById(R.id.send_parcel_ripple)).setVisibility(View.VISIBLE);

                                    } else {
                                        Toast.makeText(mContext, "No More Job available!", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class AdapterDeliverylist extends BaseAdapter {

        LinkedList<JSONObject> obj;
        Context mContext;
        int res;
        AlertDialog alertDialog;
        String StrFeedback;

        public AdapterDeliverylist(Context context, int resource, LinkedList<JSONObject> object) {
            this.mContext = context;
            this.res = resource;
            this.obj = object;

        }

        public void loadData() {
            index = index + 1;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return obj.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            try {
                JSONObject j = obj.get(position);

                viewHolder.mydelivery_date.setText(j.getString("pic_at_time"));
                viewHolder.mydelivery_dropup.setText(j.getString("dropoffaddress"));
                viewHolder.mydelivery_pickup.setText(j.getString("picupaddress"));
                viewHolder.total_distance.setText(j.getString("total_distance") + " km");
                viewHolder.mydelivery_amount.setText("$" + "\t" + j.getString("total_cost"));
                if ((j.getString("delivery_status").equals("accepting"))) {
                    viewHolder.delivery_status.setText("Parcel is accepted");
                } else if (j.getString("delivery_status").equals("pending")) {
                    viewHolder.delivery_status.setText("Pending");
                } else if (j.getString("delivery_status").equals("picking")) {
                    viewHolder.delivery_status.setText("your parcel is picked by our team");
                } else if (j.getString("delivery_status").equals("running")) {
                    viewHolder.delivery_status.setText("Running");
                } else if (j.getString("delivery_status").equals("completing")) {
                    viewHolder.delivery_status.setText("Parcel is delivered");
                }
                if ((j.getString("delivery_status").equals("completing")) && (j.getString("feedback_status").equals("0"))) {
                    viewHolder.feedback_btn.setVisibility(View.VISIBLE);
                } else if (j.getString("feedback_status").equals("1")) {
                    viewHolder.feedback_btn.setVisibility(View.INVISIBLE);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            viewHolder.feedback_btn.setOnClickListener(new MyClickFeedbackpos(position));
            return convertView;

        }

        public void AddQuantityDialog(final int pos) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
            LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View convertView = li.inflate(R.layout.feedback_dialog, null);
            alertDialogBuilder.setView(convertView);
            alertDialogBuilder.setCancelable(false);
            alertDialog = alertDialogBuilder.create();
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;

            (convertView.findViewById(R.id.send_feedback_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StrFeedback = ((CustomEditText) convertView.findViewById(R.id.feedback_description)).getText().toString();
                    if (StrFeedback.equals("")) {
                        Utility.showToast(mContext, "please provide your feedback");
                    } else if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {
                        SendFeedbackTask(pos);
                    } else {
                        Utility.ShowToastMessage(mContext, "You need to signup or login first");
                        alertDialog.cancel();
                    }

                }
            });

            (convertView.findViewById(R.id.cancel_btn)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.cancel();
                }
            });
            alertDialog.show();
        }

        public void SendFeedbackTask(final int pos) {
            progressHUD = ProgressHUD.show(mContext, true, false, null);

/*https://infograins.com/INFO01/motoxpress/api/api.php?action=UserFeedback&username=raj&feedback=default&userid=12*/

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("username", Utility.getSharedPreferences(mContext, Constants.FULLNAME));
                jsonObject.put("feedback", StrFeedback);
                jsonObject.put("driverid", Utility.getSharedPreferences(mContext, Constants.DRIVERID));
                jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
                jsonObject.put("parcelid", Utility.getSharedPreferences(mContext, Constants.ParcelID));

                responseTask = new ResponseTask(mContext, jsonObject, Constants.USERFEEDBACK, TAG, "post");
                responseTask.execute();
                responseTask.setListener(new ResponseListener() {
                    @Override
                    public void onPickSuccess(String result) {
                        if (progressHUD.isShowing()) {
                            progressHUD.dismiss();
                        }
                        if (result == null) {
                            Utility.ShowToastMessage(mContext, "Server not responding");
                        } else {
                            try {
                                JSONObject json = new JSONObject(result);
                                if (json.getString("success").equalsIgnoreCase("1")) {
                                    Utility.showToast(mContext, "Thankyou for your feedback");
                                    alertDialog.dismiss();
                                    try {
                                        JSONObject j = deliveryarraylist.get(pos);
                                        System.out.println("ARRAY POSITION " + j);
                                        j.remove("feedback_status");
                                        j.put("feedback_status", "1");
                                        deliveryarraylist.remove(pos);
                                        deliveryarraylist.add(pos, j);
                                        adapter.notifyDataSetChanged();
                                    } catch (JSONException e) {

                                    }
                                } else {
                                    Utility.showToast(mContext, json.getString("msg"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        public class MyClickFeedbackpos implements View.OnClickListener {
            int pos;

            public MyClickFeedbackpos(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    Utility.setSharedPreference(mContext, Constants.DRIVERID, obj.get(pos).getString("did"));
                    Utility.setSharedPreference(mContext, Constants.ParcelID, obj.get(pos).getString("parcelid"));
                    AddQuantityDialog(pos);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

        class ViewHolder {
            CustomTextView mydelivery_date, mydelivery_pickup, mydelivery_dropup, mydelivery_amount,
                    delivery_status, total_distance;
            CustomButton feedback_btn;

            public ViewHolder(View base) {

                mydelivery_date = (CustomTextView) base.findViewById(R.id.mydelivery_date);
                mydelivery_pickup = (CustomTextView) base.findViewById(R.id.mydelivery_pickup);
                mydelivery_dropup = (CustomTextView) base.findViewById(R.id.mydelivery_dropup);
                mydelivery_amount = (CustomTextView) base.findViewById(R.id.mydelivery_amount);
                delivery_status = (CustomTextView) base.findViewById(R.id.delivery_order);
                total_distance = (CustomTextView) base.findViewById(R.id.total_distance);
                feedback_btn = (CustomButton) base.findViewById(R.id.feedback_btn);
            }

        }
    }

}
