package com.motoxpress.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.motoxpress.NotificationdetailActivity;
import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Tejsh on 9/28/2016.
 */
public class NotificationListFragment extends Fragment {

    View rootView;
    Context mContext;
    ResponseTask responseTask;
    ArrayList<JSONObject> notificationarraylist;
    ListView noti_list;
    ProgressHUD progressHUD;
    ShimmerTextView shimmertv;
    Shimmer shimmer;
    AdapterNotificatonlist adapter;
    String TAG = NotificationListFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.noficationlist, container, false);
        mContext = getActivity();
        shimmertv = (ShimmerTextView) rootView.findViewById(R.id.notitext);
        noti_list = (ListView) rootView.findViewById(R.id.noti_list);

        Init();

        return rootView;
    }

    public void Init() {
        toggleAnimation();

    }

    public void toggleAnimation() {
        if (shimmer != null && shimmer.isAnimating()) {
            shimmer.cancel();
        } else {
            shimmer = new Shimmer();
            shimmer.start(shimmertv);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Utility.isConnectingToInternet(mContext)) {
                MyNotificationListTask();

            } else {
                Utility.showToast(mContext, "Please check your network connection");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*https://infograins.com/INFO01/motoxpress/api/api.php?action=NotificationList&parcelid=1*/

    public void MyNotificationListTask() {
        try {
            progressHUD = ProgressHUD.show(mContext, true, false, null);
            JSONObject jsonObject = new JSONObject();
            System.out.println("USERID " + Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            //jsonObject.put("userid", "37");
            responseTask = new ResponseTask(mContext, jsonObject, Constants.NOTIFICATIONLIST, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {

                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                notificationarraylist = new ArrayList<>();
                                JSONArray userdetails = json.getJSONArray("Detail");
                                for (int i = 0; i < userdetails.length(); i++) {
                                    JSONObject jo = null;
                                    jo = userdetails.getJSONObject(i);
                                    notificationarraylist.add(jo);
                                }

                                adapter = new AdapterNotificatonlist(mContext, R.layout.adapter_notificationlist, notificationarraylist);
                                noti_list.setAdapter(adapter);
                            } else {
                                if (json.getString("msg").equals("No notification Found")) {
                                    noti_list.setVisibility(View.GONE);
                                    ((ShimmerTextView) rootView.findViewById(R.id.notitext)).setVisibility(View.VISIBLE);

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public class AdapterNotificatonlist extends BaseAdapter {

        ArrayList<JSONObject> obj;
        Context mContext;
        int res;

        public AdapterNotificatonlist(Context context, int resource, ArrayList<JSONObject> object) {
            this.mContext = context;
            this.res = resource;
            this.obj = object;

        }


        @Override
        public int getCount() {
            return obj.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = li.inflate(res, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            try {
                JSONObject j = obj.get(position);
                if (j.getString("notistatus").equals("0")) {
                    viewHolder.noti_image_black.setVisibility(View.GONE);
                    viewHolder.noti_image_red.setVisibility(View.VISIBLE);
                } else if (j.getString("notistatus").equals("1")) {
                    viewHolder.noti_image_black.setVisibility(View.VISIBLE);
                    viewHolder.noti_image_red.setVisibility(View.GONE);
                }

                if ((j.getString("delivery_status").equals("accepting"))) {
                    viewHolder.notify_noti_type.setText("Parcel is accepted");
                } else if (j.getString("delivery_status").equals("pending")) {
                    viewHolder.notify_noti_type.setText("Pending");
                } else if (j.getString("delivery_status").equals("picking")) {
                    viewHolder.notify_noti_type.setText("your parcel is picked by our team");
                } else if (j.getString("delivery_status").equals("running")) {
                    viewHolder.notify_noti_type.setText("Running");
                } else if (j.getString("delivery_status").equals("completing")) {
                    viewHolder.notify_noti_type.setText("Parcel is Delivered ");
                }
                viewHolder.notify_drivername.setText(j.getString("picupcontactname"));
                if (j.getString("runnermobile").equals("null")) {
                    viewHolder.notify_driver_no.setText("");
                } else {
                    viewHolder.notify_driver_no.setText(j.getString("runnermobile"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            convertView.setOnClickListener(new MyClickpos(position));
            return convertView;
        }

        public class MyClickpos implements View.OnClickListener {
            int pos;

            public MyClickpos(int pos) {
                this.pos = pos;
            }

            @Override
            public void onClick(View v) {
                try {
                    Bundle notidetail = new Bundle();
                    Intent i = new Intent(mContext, NotificationdetailActivity.class);
                    notidetail.putString("noti_id", obj.get(pos).getString("id"));
                    notidetail.putString("parcel_id", obj.get(pos).getString("parcelid"));
                    notidetail.putString("parcel_type", obj.get(pos).getString("parcle_type"));
                    notidetail.putString("runnermobile", obj.get(pos).getString("runnermobile"));
                    notidetail.putString("delivery_status", obj.get(pos).getString("delivery_status"));
                    notidetail.putString("boking_time", obj.get(pos).getString("pic_at_time"));
                    notidetail.putString("total_cost", obj.get(pos).getString("total_cost"));
                    notidetail.putString("total_distance", obj.get(pos).getString("total_distance"));
                    notidetail.putString("pickup_address", obj.get(pos).getString("picupaddress"));
                    notidetail.putString("dropof_address", obj.get(pos).getString("dropoffaddress"));
                    notidetail.putString("noti_status", obj.get(pos).getString("notistatus"));
                    i.putExtras(notidetail);
                    startActivity(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }

        class ViewHolder {
            CustomTextView notify_parcel_type, notify_noti_type, notify_drivername, notify_driver_no;
            ImageView noti_image_black, noti_image_red;

            public ViewHolder(View base) {
                notify_parcel_type = (CustomTextView) base.findViewById(R.id.notify_parcel_type);
                notify_noti_type = (CustomTextView) base.findViewById(R.id.notify_noti_type);
                notify_drivername = (CustomTextView) base.findViewById(R.id.notify_drivername);
                noti_image_black = (ImageView) base.findViewById(R.id.noti_image_black);
                noti_image_red = (ImageView) base.findViewById(R.id.noti_image_red);
                notify_driver_no = (CustomTextView) base.findViewById(R.id.notify_driver_no);

            }

        }
    }

}
