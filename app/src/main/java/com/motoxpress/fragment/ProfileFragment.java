package com.motoxpress.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.motoxpress.ChangePasswordActivity;
import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONObject;

/**
 * Created by Tejsh on 8/23/2016.
 */
public class ProfileFragment extends Fragment {

    public String TAG = ProfileFragment.class.getSimpleName();
    Context mContext;
    View rootView;
    String name = "", email = "", mobile = "";
    CustomEditText editprofile_name, editprofile_email, editprofile_mobile;
    ResponseTask responseTask;
  ProgressHUD progressHUD;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        Find();
        Init();
        return rootView;
    }

    public void Find() {

        editprofile_mobile = ((CustomEditText) rootView.findViewById(R.id.editprofile_mobile));
        editprofile_email = ((CustomEditText) rootView.findViewById(R.id.editprofile_email));
        editprofile_name = ((CustomEditText) rootView.findViewById(R.id.editprofile_name));
        if (Utility.isConnectingToInternet(mContext)) {
            GetProfileTask();
        } else {
            Utility.ShowToastMessage(mContext, "No Internet Connection");
        }
    }

    public void Init() {
        editprofile_name.setText(Utility.getSharedPreferences(mContext, Constants.USER_NAME));
        editprofile_email.setText(Utility.getSharedPreferences(mContext, Constants.USER_EMAIL));
        editprofile_mobile.setText(Utility.getSharedPreferences(mContext, Constants.USER_MOBILE));

        ((RippleView) rootView.findViewById(R.id.ripple_changepw)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, ChangePasswordActivity.class));
            }
        });

        ((RippleView) rootView.findViewById(R.id.ripple_updateprofilebtn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                name = editprofile_name.getText().toString();
                email = editprofile_email.getText().toString();
                mobile = editprofile_mobile.getText().toString();
                if (name.equals("")) {
                    //editprofile_name.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "email not be empty");
                } else if (email.equals("")) {
                    //editprofile_email.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "email not be empty");
                } else if (!Utility.isValidEmail(email)) {
                    //editprofile_email.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "email not valid");
                } else if (mobile.equals("")) {
                    //editprofile_mobile.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "mobile number not be empty");
                } else {
                    EditProfileTask();
                }
            }
        });

    }

    public void EditProfileTask() {
        progressHUD = ProgressHUD.show(mContext, false, true,null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            jsonObject.put("fullname", name);
            jsonObject.put("email", email);
            jsonObject.put("mobile", mobile);
            responseTask = new ResponseTask(mContext, jsonObject, Constants.UPDATEUSERPROFILE, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                Utility.ShowToastMessage(mContext, "Your profile is updated...");
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetProfileTask() {
        progressHUD = ProgressHUD.show(mContext, false, true,null);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));

            responseTask = new ResponseTask(mContext, jsonObject, Constants.GetUserProfile, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {
                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {
                                JSONObject jsonObject1 = json.getJSONObject("Detail");
                                editprofile_name.setText(jsonObject1.getString("fullname"));
                                editprofile_email.setText(jsonObject1.getString("email"));
                                editprofile_mobile.setText(jsonObject1.getString("mobile"));
                            } else {
                                Utility.ShowToastMessage(mContext, json.getString("msg"));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * http://infograins.com.208-91-199-7.md-plesk-web2.webhostbox.net/INFO01/motoxpress/api/api.php?action=updateuserprofile
    * &userid=1&fullname=raju&email=fdfds@g.com&mobile=132
     */

    /*public class EditProfileTask extends AsyncTask<String, String, String> {
        String result = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            {
                updatepw_dialog.isShown();
                updatepw_dialog.setVisibility(View.VISIBLE);
            }
        }

        @Override
        protected String doInBackground(String... strings) {

            ArrayList<NameValuePair> pairArrayList = new ArrayList<NameValuePair>();
            pairArrayList.add(new BasicNameValuePair("userid", Utility.getSharedPreferences(mContext,Constants.USERID)));
            pairArrayList.add(new BasicNameValuePair("fullname", name));
            pairArrayList.add(new BasicNameValuePair("email", email));
            pairArrayList.add(new BasicNameValuePair("mobile", mobile));

            result = Utility.postParamsAndfindJSON(Constants.SERVER_URL + Constants.UPDATEUSERPROFILE, pairArrayList);

            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            updatepw_dialog.setVisibility(View.GONE);
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Server not responding");
            } else {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equalsIgnoreCase("1")) {
                        Utility.showToast(mContext, "Your profile is updated...");

                    } else {
                        Utility.ShowToastMessage(mContext, json.getString("msg"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result);
        }
    }*/
}