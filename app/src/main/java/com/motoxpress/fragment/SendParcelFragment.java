package com.motoxpress.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.motoxpress.ParcelTypeActivity;
import com.motoxpress.PickandDropAddressActivity;
import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.customwidget.CustomEditText;
import com.motoxpress.customwidget.CustomTextView;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.Locale;

/**
 * Created by LakhanPatidar on 19-Aug-16.
 */
public class SendParcelFragment extends Fragment {

    public String TAG = SendParcelFragment.class.getSimpleName();
    Context mContext;
    View rootView;
    SharedPreferences share;
    SharedPreferences.Editor edit;
    CustomTextView pickup_adress_from, drop_adress_to;
    String stringpickupaddress = "", stringdropupaddress = "", fcompanyname = "", funit = "", tcomnapny = "", tunit = "";

    double pickup_lat, pickup_long, dropup_lat, dropup_long, distanceADouble;
    ResponseTask responseTask;
    Bundle b;
    ProgressHUD progressHUD;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        share = mContext.getSharedPreferences("pref", Context.MODE_PRIVATE);
       /* if (Utility.getSharedPreferences(mContext, Constants.FCMID).equals("")) {
            GCMIntentService.registerForNotification(mContext);
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_sendparcel, container, false);
        Find();
        Init();

        return rootView;
    }

    public void Find() {

        pickup_adress_from = (CustomTextView) rootView.findViewById(R.id.pickup_adress_from);
        drop_adress_to = (CustomTextView) rootView.findViewById(R.id.pickup_adress_to);
        // sendparcel_circularprogress = (UberProgressView) rootView.findViewById(R.id.sendparcel_circularprogress);
        if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {

        } else {
            if (Utility.isConnectingToInternet(mContext)) {
                GetRegisterUserTask();
            } else {
                Utility.ShowToastMessage(mContext, "No Internet Connection");
            }
        }
    }

    public void Init() {

        ((RippleView) rootView.findViewById(R.id.ripple_next_parent)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                distanceADouble = Utility.distance(pickup_lat, pickup_long, dropup_lat, dropup_long, "K");
                fcompanyname = ((CustomEditText) rootView.findViewById(R.id.company_name_from)).getText().toString();
                funit = ((CustomEditText) rootView.findViewById(R.id.unit_name_from)).getText().toString();
                tcomnapny = ((CustomEditText) rootView.findViewById(R.id.company_name_to)).getText().toString();
                tunit = ((CustomEditText) rootView.findViewById(R.id.unit_name_to)).getText().toString();

                if (stringpickupaddress.equals("")) {
                    pickup_adress_from.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "Please add your pickup address!!!");

                } else if (stringdropupaddress.equals("")) {
                    drop_adress_to.setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "Please add your dropup address!!!");

                } else if (fcompanyname.equals("")) {
                    ((CustomEditText) rootView.findViewById(R.id.company_name_from)).
                            setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "Please add your pickup company name!!!");

                } else if (tcomnapny.equals("")) {
                    ((CustomEditText) rootView.findViewById(R.id.company_name_to)).
                            setHintTextColor(getResources().getColor(R.color.red));
                    Utility.ShowToastMessage(mContext, "Please add your drop up company name!!!");

                } else {
                    b = new Bundle();
                    b.putString(Constants.PickUpAdd, stringpickupaddress);
                    b.putString(Constants.DropOffAdd, stringdropupaddress);
                    b.putString(Constants.PickUpCompany, fcompanyname);
                    b.putString(Constants.DropOffCompany, tcomnapny);
                    b.putString(Constants.PickUpUnit, funit);
                    b.putString(Constants.DropOffUnit, tunit);
                    new GetDirectionsTask().execute(stringpickupaddress, stringdropupaddress);
                }
            }
        });
/*
        ((RippleView) rootView.findViewById(R.id.ripple_next_parent)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(mContext, ParcelTypeActivity.class));
            }
        });*/

        pickup_adress_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PickandDropAddressActivity.class);
                startActivityForResult(i, 2);
              /*  ((HomeActivity) mContext).CallSelectedFragment(Constants.PICKANDDROPADDRESSFRAGMENT, "FROM");*/
            }
        });

        drop_adress_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), PickandDropAddressActivity.class);
                startActivityForResult(i, 109);
              /*  ((HomeActivity) mContext).CallSelectedFragment(Constants.PICKANDDROPADDRESSFRAGMENT, "FROM");*/

            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2 && resultCode == Activity.RESULT_OK) {
            stringpickupaddress = data.getStringExtra("PICKADDRESS");
            pickup_adress_from.setText(stringpickupaddress);
            new GetLatLongFromAddressTask().execute(stringpickupaddress, String.valueOf(requestCode));
        } else if (requestCode == 109 && resultCode == Activity.RESULT_OK) {
            stringdropupaddress = data.getStringExtra("DROPADDRESS");
            drop_adress_to.setText(stringdropupaddress);
            new GetLatLongFromAddressTask().execute(stringdropupaddress, String.valueOf(requestCode));
        }
    }

    public void GetRegisterUserTask() {
        //https://infograins.com/INFO01/motoxpress/api/api.php?action=GuestRegister&deviceid=1234567890
        try {
            progressHUD = ProgressHUD.show(mContext, true, false, null);
            final JSONObject jsonObject = new JSONObject();
            jsonObject.put("deviceid", Utility.getSharedPreferences(mContext, Constants.FCMID));
            jsonObject.put("device_token", Utility.getSharedPreferences(mContext, Constants.IMEINUMBER));
            jsonObject.put("device_type","android");
            responseTask = new ResponseTask(SendParcelFragment.this, jsonObject, Constants.GUEST_REGISTER, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "server not responding");
                    } else if (result.equalsIgnoreCase("SocketTimeoutException")) {
                        GetRegisterUserTask();
                    } else {
                        try {
                            JSONObject jsonObject1 = new JSONObject(result);
                            if (jsonObject1.getString("success").equals("1")) {
                                Utility.setSharedPreference(mContext, Constants.USERID, jsonObject1.getString(Constants.USERID));
                                Utility.setSharedPreference(mContext, Constants.USER_TYPE, jsonObject1.getString(Constants.USER_TYPE));

                            } else {
                                Utility.ShowToastMessage(mContext, "Failed to register");
                                Log.e(TAG, "Failed to register");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception downloadUrl ==== " + e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    class GetLatLongFromAddressTask extends AsyncTask<String, String, Address> {

        String errorMessage = "";
        String requestCode = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressHUD = ProgressHUD.show(mContext, true, false, null);
        }


        @Override
        protected Address doInBackground(String... params) {
            Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocationName(params[0], 1);
                requestCode = params[1];
            } catch (IOException e) {
                errorMessage = "Service not available";
                Log.e(TAG, errorMessage, e);
            }
            if (addresses != null && addresses.size() > 0) {
                return addresses.get(0);
            } else {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Address address) {
            if (!progressHUD.equals(null) && progressHUD.isShowing()) {
                progressHUD.dismiss();
            }
            if (address == null) {

            } else {
                try {
                    String addressName = "";
                    for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressName += " --- " + address.getAddressLine(i);
                    }
                    Log.e(TAG, "Latitude: " + address.getLatitude() + "\n" + "Longitude: " + address.getLongitude() + "\n" +
                            "Address: " + addressName);
                    Log.e(TAG, "newlat === " + address.getLatitude());
                    Log.e(TAG, "newlong === " + address.getLongitude());
                    if (requestCode.equals("2")) {
                        pickup_lat = address.getLatitude();
                        pickup_long = address.getLongitude();
                    } else if (requestCode.equals("109")) {
                        dropup_lat = address.getLatitude();
                        dropup_long = address.getLongitude();
                    }
                } catch (Exception e) {
                    Log.e(TAG, "onPostExecute Exception ====== " + e.toString());
                    e.printStackTrace();
                }
            }
            super.onPostExecute(address);
        }
    }

    class GetDirectionsTask extends AsyncTask<String, String, String> {

        String result = "";

        @Override
        protected String doInBackground(String... params) {
            try {
                //http://maps.googleapis.com/maps/api/directions/json?origin=&destination=&sensor=false
                String key = "key=AIzaSyDjWAHFU1CmsLz8CYulaK9rSPMWlJW2JZg";
                String origin = "";
                origin = "origin=" + URLEncoder.encode(params[0], "utf-8");
                String destination = "destination=" + URLEncoder.encode(params[1], "utf-8");
                String sensor = "sensor=false";
                String parameters = origin + "&" + destination + "&" + sensor + "&" + key;

                String output = "json";
                String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
                Log.e(TAG, "GetDirectionsTask ====== " + url);
                result = downloadUrl(url);
            } catch (UnsupportedEncodingException e1) {
                Log.d(TAG, "UnsupportedEncodingException GetDirectionsTask ====== " + e1.toString());
                e1.printStackTrace();
            } catch (Exception e) {
                Log.d(TAG, "Exception GetDirectionsTask ====== " + e.toString());
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            if (result == null) {
                Utility.ShowToastMessage(mContext, "Location not found at this time.");
            } else {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray legs = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs");
                    JSONObject distance = legs.getJSONObject(0).getJSONObject("distance");
                    JSONObject end_location = legs.getJSONObject(0).getJSONObject("end_location");
                    JSONObject start_location = legs.getJSONObject(0).getJSONObject("start_location");

                    b.putString(Constants.PickUpLat, start_location.getString("lat"));
                    b.putString(Constants.PickUpLong, start_location.getString("lng"));
                    b.putString(Constants.DropOffLat, end_location.getString("lat"));
                    b.putString(Constants.DropOffLong, end_location.getString("lng"));
                    String[] token = distance.getString("text").split(" ");
                    b.putString(Constants.Distance, token[0]);
                    Intent i = new Intent(mContext, ParcelTypeActivity.class);
                    i.putExtras(b);
                    startActivity(i);
                } catch (Exception e) {
                    Log.e(TAG, "onPostExecute Exception ====== " + e.toString());
                    e.printStackTrace();
                }
            }
            super.onPostExecute(result);
        }
    }

}


