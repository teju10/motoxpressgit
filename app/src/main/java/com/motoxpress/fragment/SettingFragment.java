package com.motoxpress.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.kyleduo.switchbutton.SwitchButton;
import com.motoxpress.R;
import com.motoxpress.constants.Constants;
import com.motoxpress.utility.ProgressHUD;
import com.motoxpress.utility.ResponseListener;
import com.motoxpress.utility.ResponseTask;
import com.motoxpress.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Infograins on 9/28/2016.
 */
public class SettingFragment extends Fragment {

    Context mContext;
    View rootView;
    Switch switch_notification;
    ProgressHUD progressHUD;
    ResponseTask responseTask;
    String TAG = "SettingFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_setting, container, false);
        mContext = getActivity();
        Init();
        return rootView;
    }

    public void Init() {
        if (Utility.getSharedPreferences(mContext, Constants.NOTIFICATION_STATUS).equals("1")) {
            ((SwitchButton) rootView.findViewById(R.id.switch_notification)).setChecked(true);
        } else if (Utility.getSharedPreferences(mContext, Constants.NOTIFICATION_STATUS).equals("0")) {
            ((SwitchButton) rootView.findViewById(R.id.switch_notification)).setChecked(false);
        }

        ((SwitchButton) rootView.findViewById(R.id.switch_notification)).setOnCheckedChangeListener(new CompoundButton.
                OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (Utility.getSharedPreferences(mContext, Constants.SPLOGIN).equals("1")) {
                    StopNotificationStatusTask();
                }
                else  {
                    ((SwitchButton) rootView.findViewById(R.id.switch_notification)).setChecked(true);
                    Utility.ShowToastMessage(mContext, "You need to login or sigup first");
                }
            }
        });


        /*((RippleView) rootView.findViewById(R.id.update_notification_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utility.showToast(mContext, "Coming soon");
            }
        });*/

    }

    public void StopNotificationStatusTask() {
        try {
            progressHUD = ProgressHUD.show(mContext, true, false, null);
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("userid", Utility.getSharedPreferences(mContext, Constants.USERID));
            if (Utility.getSharedPreferences(mContext, Constants.NOTIFICATION_STATUS).equals("1")) {
                jsonObject.put("status", "0");
            } else if (Utility.getSharedPreferences(mContext, Constants.NOTIFICATION_STATUS).equals("0")) {
                jsonObject.put("status", "1");
            }
            responseTask = new ResponseTask(mContext, jsonObject, Constants.NOTIFICATIONSTATUS, TAG, "post");
            responseTask.execute();
            responseTask.setListener(new ResponseListener() {
                @Override
                public void onPickSuccess(String result) {
                    if (!progressHUD.equals("") && progressHUD.isShowing()) {
                        progressHUD.dismiss();
                    }
                    if (result == null) {
                        Utility.ShowToastMessage(mContext, "Server not responding");
                    } else {
                        try {

                            JSONObject json = new JSONObject(result);
                            if (json.getString("success").equalsIgnoreCase("1")) {

                                if (json.getString("noti_status").equals("0")) {
                                    Utility.setSharedPreference(mContext, Constants.NOTIFICATION_STATUS, json.getString("noti_status"));
                                    Utility.showToast(mContext, "Notification is stop");
                                } else if (json.getString("msg").equals("Notification on")) {
                                    Utility.setSharedPreference(mContext, Constants.NOTIFICATION_STATUS, json.getString("noti_status"));
                                    Utility.showToast(mContext, "Notification is on");
                                }

                            } else {
                                Utility.showToast(mContext, json.getString("msg"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
