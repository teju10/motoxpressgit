package com.motoxpress.navdrawer;

/**
 * Created by Ravi on 29/07/15.
 */
public class NavDrawerItem {
    public boolean showNotify;
    public String title;
    public int image;

    public NavDrawerItem() {

    }

    public NavDrawerItem(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int images) {
        this.image = images;
    }
}
