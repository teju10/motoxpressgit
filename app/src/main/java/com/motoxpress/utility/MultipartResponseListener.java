package com.motoxpress.utility;

/**
 * Created by Nilesh Kansal on 05-Aug-16.
 */
public interface MultipartResponseListener {

    void onPickSuccess(String result);
}