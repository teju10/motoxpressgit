package com.motoxpress.utility;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.motoxpress.constants.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Iterator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by Nilesh Kansal on 05-Aug-16.
 */
public class MultipartTask extends AsyncTask<String, String, String> {

    String result = "", action = "", TAG = "", method = "", fileName = "", fileParam = "";
    Context mContext;
    JSONObject params;
    Fragment fragment;
    Activity activity;
    MultipartResponseListener mListener;

    public MultipartTask(Activity activity, JSONObject parameters, String URL_ACTION, String FileParam, String FileName,
                         String TAG, String METHOD) {
        this.activity = activity;
        this.params = parameters;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public MultipartTask(Activity activity, String URL_ACTION, String FileParam, String FileName, String TAG, String METHOD) {
        this.activity = activity;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public MultipartTask(Context context, JSONObject parameters, String URL_ACTION, String FileParam, String FileName,
                         String TAG, String METHOD) {
        this.mContext = context;
        this.params = parameters;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public MultipartTask(Context context, String URL_ACTION, String FileParam, String FileName, String TAG, String METHOD) {
        this.mContext = context;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public MultipartTask(Fragment fragment, JSONObject parameters, String URL_ACTION, String FileParam, String FileName,
                         String TAG, String METHOD) {
        this.fragment = fragment;
        this.params = parameters;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public MultipartTask(Fragment fragment, String URL_ACTION, String FileParam, String FileName, String TAG, String METHOD) {
        this.fragment = fragment;
        this.action = URL_ACTION;
        this.fileName = FileName;
        this.fileParam = FileParam;
        this.TAG = TAG;
        this.method = METHOD;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            if (method.equalsIgnoreCase("onlyFile")) {
                MultipartUtility mu;
                mu = new MultipartUtility(Constants.SERVER_URL + action, "UTF-8");
                if (fileName != null && !fileName.equals("")) {
                    mu.addFilePart(fileParam, fileName);
                }
                result = mu.finish();
            } else {
                MultipartUtility mu;
                mu = new MultipartUtility(Constants.SERVER_URL + action, "UTF-8");

                Iterator<String> keys = params.keys();
                while (keys.hasNext()) {
                    try {
                        String id = String.valueOf(keys.next());
                        mu.addFormField(id, "" + params.get(id));
                        System.out.println(id + " : " + params.get(id));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (fileName != null && !fileName.equals("")) {
                    mu.addFilePart(fileParam, fileName);
                }
                result = mu.finish();
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = null;
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e(TAG, "result ===== " + s);
        try {
            returnResult(s);
        } catch (Exception e) {
            e.printStackTrace();
            returnResult(null);
        }
    }

    public void setListener(MultipartResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {
        if (mListener != null) {
            mListener.onPickSuccess(result);
        }
    }

    public static SSLSocketFactory createSslSocketFactory() throws Exception {
        TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, byPassTrustManagers, new SecureRandom());
        return sslContext.getSocketFactory();
    }

    public class MultipartUtility {

        private static final String LINE_FEED = "\r\n";
        private String boundary = "";
        String response = null;
        private HttpURLConnection httpConn;
        private HttpsURLConnection httpsConn;
        private String charset, RequestURL;
        private OutputStream outputStream;
        private PrintWriter writer;

        /**
         * This constructor initializes a new HTTP POST request with content type
         * is set to multipart/form-data
         *
         * @param requestURL
         * @param charset
         * @throws IOException
         */

        public MultipartUtility(String requestURL, String charset) throws IOException {

            Log.e("MultipartUtility", requestURL);
            RequestURL = requestURL;
            this.charset = charset;
            try{
                // creates a unique boundary based on time stamp
                boundary = "===" + System.currentTimeMillis() + "===";
                if(RequestURL.contains("https")){
                    URL url = new URL(requestURL);
                    httpsConn = (HttpsURLConnection) url.openConnection();
                    httpsConn.setDoOutput(true);    // indicates POST method
                    httpsConn.setDoInput(true);
                    httpsConn.setConnectTimeout(20000);
                    SSLSocketFactory sslSocketFactory = createSslSocketFactory();
                    httpsConn.setSSLSocketFactory(sslSocketFactory);
                    httpsConn.setHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            return true;
                        }
                    });
                    httpsConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                    outputStream = httpConn.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
                } else {
                    URL url = new URL(requestURL);
                    httpConn = (HttpURLConnection) url.openConnection();
                    httpConn.setDoOutput(true);    // indicates POST method
                    httpConn.setDoInput(true);
                    httpConn.setConnectTimeout(20000);
                    httpConn.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
                    outputStream = httpConn.getOutputStream();
                    writer = new PrintWriter(new OutputStreamWriter(outputStream, charset), true);
                }
            } catch (SocketTimeoutException e){
                e.printStackTrace();
                response = null;
            } catch (SSLException e){
                e.printStackTrace();
                response = null;
            } catch (Exception e){
                e.printStackTrace();
                response = null;
            }
        }

        /**
         * Adds a form field to the request
         *
         * @param name  field name
         * @param value field value
         */
        public void addFormField(String name, String value) {
            Log.e(TAG, "addFormField ====== " + name + " = " + value);
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + name + "\"").append(LINE_FEED);
            writer.append("Content-Type: text/plain; charset=" + charset).append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.append(value).append(LINE_FEED);
            writer.flush();
        }

        /**
         * Adds a upload file section to the request
         *
         * @param fieldName name attribute in <input type="file" name="..." />
         * @throws IOException
         */
        public void addFilePart(String fieldName, String filename) throws IOException {
            String fileName = filename;
            Log.e(TAG, "addFilePart ====== " + fieldName + " = " + filename);
            writer.append("--" + boundary).append(LINE_FEED);
            writer.append("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"")
                    .append(LINE_FEED);
            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(fileName)).append(LINE_FEED);
            writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
            writer.append(LINE_FEED);
            writer.flush();
            FileInputStream inputStream = new FileInputStream(filename);
            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
            inputStream.close();
            writer.append(LINE_FEED);
            writer.flush();
        }

        /**
         * Completes the request and receives response from the server.
         *
         * @return a list of Strings as response in case the server returned
         * status OK, otherwise an exception is thrown.
         * @throws IOException
         */

        public String finish() throws IOException {
            Log.e(TAG, "finish()");
            writer.append(LINE_FEED).flush();
            writer.append("--" + boundary + "--").append(LINE_FEED);
            writer.close();

            // checks server's status code first
            if(RequestURL.contains("https")){
                int status = httpsConn.getResponseCode();

                System.out.println("response from multipart class......................" + status);

                if (status == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(httpsConn.getInputStream()));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        response = line;
                        Log.e("response", line);
                    }
                    reader.close();
                    httpsConn.disconnect();
                } else {
                    Log.e(TAG, "HTTP-ERROR ==== " + httpsConn.toString());
                    throw new IOException("Server returned non-OK status: " + status);
                }
            } else {
                int status = httpConn.getResponseCode();

                System.out.println("response from multipart class......................" + status);

                if (status == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        response = line;
                        Log.e("response", line);
                    }
                    reader.close();
                    httpConn.disconnect();
                } else {
                    Log.e(TAG, "HTTP-ERROR ==== " + httpConn.toString());
                    throw new IOException("Server returned non-OK status: " + status);
                }
            }
            return response;
        }
    }
}