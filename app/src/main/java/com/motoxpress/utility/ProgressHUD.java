package com.motoxpress.utility;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager.BadTokenException;

import com.motoxpress.R;

public class ProgressHUD extends Dialog {
	public ProgressHUD(Context context) {
		super(context);
	}

	public ProgressHUD(Context context, int theme) {
		super(context, theme);
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.progress_hud);}
	@SuppressWarnings("deprecation")
	public static ProgressHUD show(Context context,
			boolean indeterminate, boolean cancelable,
			OnCancelListener cancelListener) {

		ProgressHUD dialog = new ProgressHUD(context, R.style.MyDialog);
		dialog.setCancelable(cancelable);
		dialog.setOnCancelListener(cancelListener);
		dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
		try {
			dialog.show();
		} catch (BadTokenException e) {
			e.printStackTrace();
		}
		return dialog;
	}
}
