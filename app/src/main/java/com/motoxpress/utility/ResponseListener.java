package com.motoxpress.utility;

/**
 * Created by Nilesh Kansal on 01-Aug-16.
 */
public interface ResponseListener {

    void onPickSuccess(String result);
}