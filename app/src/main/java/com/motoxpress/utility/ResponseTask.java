package com.motoxpress.utility;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.motoxpress.constants.Constants;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;





/*import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSocketFactory;*/

/**
 * Created by Nilesh Kansal on 01-Aug-16.
 */
public class ResponseTask extends AsyncTask<String, String, String> {
    String result = "", action = "";
    Context mContext;
    String TAG = "", method = "";
    JSONObject params;
    Fragment fragment;
    Activity activity;
    ResponseListener mListener;

    public ResponseTask(Activity activity, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.activity = activity;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Activity activity, String URL_ACTION, String TAG, String METHOD) {
        this.activity = activity;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Context context, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.mContext = context;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Context context, String URL_ACTION, String TAG, String METHOD) {
        this.mContext = context;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Fragment fragment, JSONObject parameters, String URL_ACTION, String TAG, String METHOD) {
        this.fragment = fragment;
        this.params = parameters;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    public ResponseTask(Fragment fragment, String URL_ACTION, String TAG, String METHOD) {
        this.fragment = fragment;
        this.action = URL_ACTION;
        this.TAG = TAG;
        this.method = METHOD;
    }

    /*public static SSLSocketFactory createSslSocketFactory() throws Exception {
        TrustManager[] byPassTrustManagers = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] chain, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] chain, String authType) {
            }
        }};
        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, byPassTrustManagers, new SecureRandom());
        return sslContext.getSocketFactory();
    }
*/
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        if (method.equalsIgnoreCase("GET")) {
            result = FindJSONFromUrl(Constants.SERVER_URL + action);
        } else {
            result = PostParamsAndFindJson(Constants.SERVER_URL + action, params);
        }
        return result;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        Log.e(TAG, "result ===== " + s);
        try {
            returnResult(s);
        } catch (Exception e) {
            e.printStackTrace();
            returnResult(null);
        }
    }

    public void setListener(ResponseListener listener) {
        mListener = listener;
    }

    private void returnResult(String result) {
        if (mListener != null) {
            mListener.onPickSuccess(result);
        }
    }

    public String FindJSONFromUrl(String targetURL) {
        StringBuffer response = new StringBuffer();

        System.out.println("URL comes in json parser class is ========= " + targetURL);
        try {
            URL myURL = new URL(targetURL);
            URLConnection urlConnection;
            HttpURLConnection httpsURLConnection;
         //   SSLSocketFactory sslSocketFactory;
            BufferedReader in;
            String inputLine;
            if (targetURL.contains("http")) {
                urlConnection = myURL.openConnection();
                httpsURLConnection = (HttpsURLConnection) urlConnection;
                /*sslSocketFactory = createSslSocketFactory();
                httpsURLConnection.setSSLSocketFactory(sslSocketFactory);
                httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });
*/
                httpsURLConnection.setReadTimeout(10000); /* milliseconds */
                httpsURLConnection.setConnectTimeout(10000); /* milliseconds */
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setUseCaches(false);
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStream os = httpsURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                writer.close();
                os.close();
                httpsURLConnection.connect();
                int responseStatus = httpsURLConnection.getResponseCode();
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getErrorStream()));
                }
            } else {
                HttpURLConnection httpURLConnection = (HttpURLConnection) (myURL).openConnection();
                httpURLConnection.setReadTimeout(10000); // milliseconds
                httpURLConnection.setConnectTimeout(10000); // milliseconds
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                writer.close();
                os.close();
                httpURLConnection.connect();
                int responseStatus = httpURLConnection.getResponseCode();
                System.out.println("content encoding in json parser class ........" + httpURLConnection.getContentEncoding());
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                }
            }

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("result in json parser class ........" + response.toString());
            return response.toString();
        } catch (SocketTimeoutException e) {
            System.out.println("SocketTimeoutException in jsonparser class ........");
            e.printStackTrace();
            return "SocketTimeoutException";
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException in jsonparser class ........");
            e.printStackTrace();
            return null;
        }/* catch (SSLException e) {
            System.out.println("SSLException in jsonparser class ........");
            e.printStackTrace();
            return null;
        }*/ catch (Exception e) {
            System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return null;
        }
    }

    public String PostParamsAndFindJson(String targetURL, JSONObject params) {
        StringBuffer response = new StringBuffer();
        System.out.println("URL comes in json parser class is:  " + targetURL + " " + params);
        try {
            URL myURL = new URL(targetURL);
            URLConnection urlConnection;
            HttpURLConnection httpsURLConnection;
           // SSLSocketFactory sslSocketFactory;
            BufferedReader in;
            String inputLine;
            if (targetURL.contains("http")) {
                urlConnection = myURL.openConnection();
                httpsURLConnection = (HttpURLConnection) urlConnection;
                /*sslSocketFactory = createSslSocketFactory();
                httpsURLConnection.setSSLSocketFactory(sslSocketFactory);*/
               /* httpsURLConnection.setHostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                });*/

                httpsURLConnection.setReadTimeout(10000); // milliseconds
                httpsURLConnection.setConnectTimeout(10000); //  milliseconds
                httpsURLConnection.setRequestMethod("GET");
                httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpsURLConnection.setUseCaches(false);
                httpsURLConnection.setDoInput(true);
                httpsURLConnection.setDoOutput(true);
                OutputStream os = httpsURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                writer.close();
                os.close();
                httpsURLConnection.connect();
                int responseStatus = httpsURLConnection.getResponseCode();
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpsURLConnection.getErrorStream()));
                }
            } else {
                HttpURLConnection httpURLConnection = (HttpURLConnection) (myURL).openConnection();
              /*  httpURLConnection.setReadTimeout(15000); *//* milliseconds *//*
                httpURLConnection.setConnectTimeout(15000); *//* milliseconds */
                httpURLConnection.setChunkedStreamingMode(0);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setDoOutput(true);
                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(params));
                writer.flush();
                writer.close();
                os.close();
                httpURLConnection.connect();
                int responseStatus = httpURLConnection.getResponseCode();
                System.out.println("status in json parser class ........" + responseStatus);
                if (responseStatus == HttpURLConnection.HTTP_OK) {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                } else {
                    in = new BufferedReader(new InputStreamReader(httpURLConnection.getErrorStream()));
                }
            }
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            System.out.println("result in json parser class ........" + response.toString());
            return response.toString();
        } catch (SocketTimeoutException e) {
            System.out.println("SocketTimeoutException in jsonparser class ........");
            e.printStackTrace();
            return "SocketTimeoutException";
        } catch (UnknownHostException e) {
            System.out.println("UnknownHostException in jsonparser class ........");
            e.printStackTrace();
            return null;
        } /*catch (SSLException e) {
            System.out.println("SSLException in jsonparser class ........");
            e.printStackTrace();
            return null;
        } */catch (Exception e) {
            System.out.println("exception in jsonparser class ........");
            e.printStackTrace();
            return null;
        }
    }

    public String getPostDataString(JSONObject params) throws Exception {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        Iterator<String> itr = params.keys();
        while (itr.hasNext()) {
            String key = itr.next();
            Object value = params.get(key);

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value.toString(), "UTF-8"));

        }
        return result.toString();
    }
}